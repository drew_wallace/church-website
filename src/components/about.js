import React, { Fragment, Component } from 'react';
import { connect } from 'react-redux';
import DccNavBar from './dccNavBar';
import DccFooter from './dccFooter';

const About = () => (
    <Fragment>
        <DccNavBar/>
        <div className="page-title about">
            <div className="page-title-overlay-container">
                <div className="page-title-overlay"></div>
            </div>
            <div className="page-title-text-container">
                <h1 className="text-center page-title-text">ABOUT DCC</h1>
            </div>
        </div>
        <div className="split-pane-container">
            <div className="split-pane-left">
                <h2 className="text-center">DCC Is About Discipleship</h2>
                <p className="text-center">Being <b>with Jesus</b></p><p className="text-center">Learning to <b>be like Jesus</b></p><p className="text-center">In order to <b>do what Jesus did</b></p>
                <hr/>
                <h4 className="bold">Being With Jesus</h4>
                <p>Scripture and prayer are tools for being with Jesus. Reading the inspired Word He gave us and talking to Him regularly are the ways we learn to walk daily with God. On Sunday morning we seek God corporately, being with Him together and encouraging one another to be with Him individually during the week. These are the first two of our seven Discipleship Marks:</p>
                <ul>
                    <li>Love of Scripture: We meet the living God in the Word of God.</li>
                    <li>Posture of Prayer: We know that apart from God we can do nothing.</li>
                </ul>
                <hr/>
                <h4 className="bold">Becoming Like Jesus</h4>
                <p>As we spend more time with Jesus, we slowly begin to take on His mannerisms and attitudes about the world. We begin to surrender our lives fully to Him and to love each other in the selfless ways to which Jesus calls us. Joining a small group provides accountability and encouragement to grow in these, the third and fourth of our Discipleship Marks:</p>
                <ul>
                    <li>Holistic Worship: We offer God all of who we are and all of what we have.</li>
                    <li>Authentic Community: We love each other in spite of our flaws.</li>
                </ul>
                <hr/>
                <h4 className="bold">Doing What Jesus Did</h4>
                <p>We seek to find the places where the Kingdom is moving and join in with what God is doing. Whether that is at DCC in a ministry team, in the workplace talking to coworkers, or overseas serving on a missions team, we want to equip and empower our members to do what Jesus did: make disciples who make disciples. These are the final three of our seven Discipleship Marks:</p>
                <ul>
                    <li>Intentional Discipleship: We are called to be disciples and to make disciples.</li>
                    <li>Sacrificial Service: We disadvantage ourselves for the sake of others.</li>
                    <li>Missional Mindset: We are sent by God to help redeem the world.</li>
                </ul><a name="docs"></a>
                <hr/>
                <h2 className="text-center">Church Documents</h2>
                <p>For more information and greater detail about what our church believes and what we consider essential, please see the following documents.</p>
                <p className="text-center"><a target="_blank" href="https://dccsiteimages.s3.amazonaws.com/files/Essentials_of_our_Faith.pdf">Statement Of Faith</a></p>
                <p className="text-center"><a target="_blank" href="https://dccsiteimages.s3.amazonaws.com/files/Membership_Covenant.pdf">Membership Covenant</a></p>
                <p className="text-center"><a target="_blank" href="https://dccsiteimages.s3.amazonaws.com/files/Discipleship_Marks.pdf">Discipleship Marks</a></p>
                <p className="text-center"><a target="_blank" href="https://dccsiteimages.s3.amazonaws.com/files/Constitution.pdf">Church Constitution</a></p>
                <p className="text-center"><a target="_blank" href="https://dccsiteimages.s3.amazonaws.com/files/Ministry_Principles_Processes.pdf">Ministry Principles &amp; Processes</a></p>
                <hr/>
                <div>
                    <h2 className="text-center">Time and Location</h2>
                    <div className="small-group-container">
                      <div className="map-image-container">
                      <a target="_blank" href="https://www.google.com/maps/place/2250+Dunwoody+Club+Dr,+Atlanta,+GA+30350/"><img width="220px" srcSet="https://dccsiteimages.s3.amazonaws.com/map.jpg"/></a>
                      </div>
                      <div className="small-group-info-container">
                        <p><b>Our regular Sunday Service begins at 10:30 AM.</b><br/>
                        There's a Children's program and a Nursery for the young ones.<br/>
                        A Cry Room is available as well in the back of the sanctuary.</p>
                        <p>We are located at:<br/>
                           <b>2250 Dunwoody Club Dr.<br/>Atlanta, GA 30350</b></p>
                        <p>Phone: <a href="tel:7703968600">770-396-8600</a><br/>
                           Email: <a href="mailto:info@dunwoodychurch.org">info@dunwoodychurch.org</a></p>
                        <p>Map: <a target="_blank" href="https://www.google.com/maps/place/2250+Dunwoody+Club+Dr,+Atlanta,+GA+30350/">2250 Dunwoody Club Dr., Atlanta, GA 30350-5408</a></p>
                        <p>The church is open Monday - Friday from 9:00am to 3:00pm. You can leave a message any time on our voice mail, and we will respond to you as soon as possible.</p>
                      </div>
                    </div>
                </div>

            </div>
            <div className="split-pane-right staff">
              <div className="staff">
                <h2 className="text-center">Staff Team</h2>
                <hr />
                <div className="staff-container">
                    <div className="staff-image-container">
                        <img srcSet="https://dccsiteimages.s3.amazonaws.com/staff/jansen.jpg" />
                    </div>
                    <div className="staff-content-container">
                        <h3 className="staff-name">JEFF JANSEN</h3>
                        <div>TEACHING PASTOR</div>
                        <div>
                            <a href="mailto:jjansen@dunwoodychurch.org">JJANSEN@DUNWOODYCHURCH.ORG</a>
                        </div>
                        <p>Jeff has been on staff at DCC since the summer of 2012 but is a Dunwoody native. Before coming on staff, he spent almost 20 years traveling the world as a missionary with Wycliffe Bible Translators. Jeff is married to Elizabeth and has three children, Nicholas, Christian, and Christina.</p>
                    </div>
                </div>
                <hr  className="dashed"/>
                <div className="staff-container">
                    <div className="staff-image-container">
                        <img srcSet="https://dccsiteimages.s3.amazonaws.com/staff/Tim.jpg" />
                    </div>
                    <div className="staff-content-container">
                        <h3 className="staff-name">TIM BEARD</h3>
                        <div>EXECUTIVE PASTOR</div>
                        <div>
                            <a href="mailto:tbeard@dunwoodychurch.org">TBEARD@DUNWOODYCHURCH.ORG</a>
                        </div>
                        <p>After twenty years in numerous pastoral roles at Fellowship Bible Church, the Lord brought Tim to DCC in April, 2018 as the church&rsquo;s Executive Pastor.  Tim and his wife Cindy have two adult children, Zak and Lauren.</p>
                    </div>
                </div>
                <hr  className="dashed"/>
                <div className="staff-container">
                    <div className="staff-image-container">
                        <img srcSet="https://dccsiteimages.s3.amazonaws.com/staff/Jordan.jpg" />
                    </div>
                    <div className="staff-content-container">
                        <h3 className="staff-name">JORDAN YOUNG</h3>
                        <div>CHILDREN & YOUTH PASTOR</div>
                        <div>
                            <a href="mailto:jyoung@dunwoodychurch.org">JYOUNG@DUNWOODYCHURCH.ORG</a>
                        </div>
                        <p>Jordan came to DCC with her parents, Mike and Elizabeth Shetler, in 1996 and grew up in the church. Jordan graduated from Kennesaw State University in May, 2016 with a degree in Secondary English Education and joined the staff team that August as the Youth Pastor.  Jordan has been married to Jeremy since the fall of 2018.</p>
                    </div>
                </div>
                <hr  className="dashed"/>
                <div className="staff-container">
                    <div className="staff-image-container">
                        <img srcSet="https://dccsiteimages.s3.amazonaws.com/staff/Jarrod.jpg" />
                    </div>
                    <div className="staff-content-container">
                        <h3 className="staff-name">JARROD KNABEL</h3>
                        <div>TECHNICAL DIRECTOR & FACILITIES MANAGER</div>
                        <div>
                            <a href="mailto:jknabel@dunwoodychurch.org">JKNABEL@DUNWOODYCHURCH.ORG</a>
                        </div>
                        <p>Jarrod has been on staff at DCC since the spring of 2003. Originally the church&rsquo;s "Sound Guy," Jarrod now handles much of the church&rsquo;s technical and physical needs including audio/visual, sanctuary/stage setup, etc. In addition to working at DCC he runs <a target="_blank" href="http://www.elevenaudio.com/">Eleven Audio</a> studios.</p>
                    </div>
                </div>
                <hr  className="dashed"/>
                <div className="staff-container">
                    <div className="staff-image-container">
                        <img srcSet="https://dccsiteimages.s3.amazonaws.com/staff/Cheri.jpg" />
                    </div>
                    <div className="staff-content-container">
                        <h3 className="staff-name">CHERI COWAN</h3>
                        <div>ADMINISTRATOR</div>
                        <div>
                            <a href="mailto:ccowan@dunwoodychurch.org">CCOWAN@DUNWOODYCHURCH.ORG</a>
                        </div>
                        <p>Cheri and her husband, Kevin, are long time Dunwoody residents as well as long time members of Dunwoody Community Church.  This is Cheri's second time working as the Church Administrator.  She had previously worked for DCC for a year filling in as the administrator when our former Executive Pastor was sick with cancer. We're excited to have Cheri back! 
						</p>
                    </div>
                </div>
                <hr  className="dashed"/>
                <div className="staff-container">
                    <div className="staff-image-container">
                        <img srcSet="https://dccsiteimages.s3.amazonaws.com/staff/Natalie.jpg" />
                    </div>
                    <div className="staff-content-container">
                        <h3 className="staff-name">NATALIE GARCIA</h3>
                        <div>WORSHIP & SOCIAL MEDIA DIRECTOR</div>
                        <div>
                            <a href="mailto:ngarciadunwoodychurch.org">NGARCIA@DUNWOODYCHURCH.ORG</a>
                        </div>
                        <p>Natalie first visited DCC while being an a Worship Intern at under Tim Beard to lead worship. Natalie also majored in Vocal Performance while in college, but ministry was always where she felt God’s call. After serving in various worship ministries the Lord called her and her family back to DCC, where she now serves as the Worship and Social Media Director. She has been happily married since May 2013 and has two daughters.</p>
                    </div>
                </div>
              </div>
              <hr className="thick"/>
              <div className="elders">
                <h2 className="text-center">Elders</h2>
                <p>DCC has adopted the model of an Elder led church. Throughout the New Testament Elders are the recognized office for leading the church. Our Elders provide oversight and direction for the ministry of DCC. You can email the DCC elders <a href="mailto:elders@dunwoodychurch.org">here</a> if you'd like to contact them for any reason.</p>
                <hr />
                <div className="staff-container">
                    <div className="staff-image-container">
                        <img srcSet="https://dccsiteimages.s3.amazonaws.com/elders/talbee.jpg" />
                    </div>
                    <div className="staff-content-container">
                        <h3 className="staff-name">TODD ALBEE</h3>
                        <p><i>Coming Soon</i></p>
                    </div>
                </div>
                
                <hr className="dashed"/>
                <div className="staff-container">
                    <div className="staff-image-container">
                        <img srcSet="https://dccsiteimages.s3.amazonaws.com/elders/gilmore.jpg" />
                    </div>
                    <div className="staff-content-container">
                        <h3 className="staff-name">DAVE GILMORE</h3>
                        <p>Dave Gilmore and his wife LuAnn have been attending DCC since 2007. He has been serving as an elder since the spring of 2013. Dave serves as the Managing Direction of the Tricord Group. When not at work Dave enjoys writing, gardening, and spending time with his grandkids.</p>
                    </div>
                </div>
                
                <hr className="dashed"/>
                <div className="staff-container">
                    <div className="staff-image-container">
                        <img srcSet="https://dccsiteimages.s3.amazonaws.com/elders/haron.jpg" />
                    </div>
                    <div className="staff-content-container">
                        <h3 className="staff-name">LARRY HARON</h3>
                         <p>Larry and Pam Haron have been DCC members since 2012. Originally from Dallas, Texas, Larry has served in several churches as worship pastor. He is an ordained minister, graduate of Dallas Theological Seminary and Southwestern Baptist Theological Seminary. A past Dove and Grammy nominated music arranger/producer, he currently loves serving as a hospice chaplain with Agape Hospice Care and is a certified music practitioner, providing therapeutic music to hospice and hospital patients. Pam’s two grown children reside in the Atlanta area and Larry’s in Dallas and New York City. </p>
                    </div>
                </div>
                
                 <hr className="dashed"/>
                 <div className="staff-container">
                    <div className="staff-image-container">
                        <img srcSet="https://dccsiteimages.s3.amazonaws.com/elders/hart.jpg" />
                    </div>
                    <div className="staff-content-container">
                        <h3 className="staff-name">BOB HART</h3>
                        <p>Bob and his wife Nancy have been attending DCC since 2010. He has been an elder since the spring of 2013. He and Nancy have been married over 30 years and have two grown children: Robert and Rachel. For nearly 25 years Bob has been a money manager and investment specialist serving both individuals and large companies.</p>
                    </div>
                </div>
                
                <hr className="dashed"/>
                <div className="staff-container">
                    <div className="staff-image-container">
                        <img srcSet="https://dccsiteimages.s3.amazonaws.com/elders/jansen.jpg" />
                    </div>
                    <div className="staff-content-container">
                        <h3 className="staff-name">JEFF JANSEN</h3>
                        <p>Jeff has been on staff at DCC since the summer of 2012 but is a Dunwoody native. Before coming on staff, he spent almost 20 years traveling the world as a missionary with Wycliffe Bible Translators. Jeff is married to Elizabeth and has three children, Nicholas, Christian, and Christina.</p>
                    </div>
                </div>
                
                <hr className="dashed"/>
                <div className="staff-container">
                    <div className="staff-image-container">
                        <img srcSet="https://dccsiteimages.s3.amazonaws.com/elders/martin.jpg"/>
                    </div>
                    <div className="staff-content-container">
                        <h3 className="staff-name">MART MARTIN</h3>
                        <p>Mart and his family have been members of DCC since 2005, and he was named an elder in 2011. He is the founder of <a href="https://3keyquestions.com/" target="_blank">3 Key Questions, LLC</a>, which helps organizations and individuals gain clarity around their purpose, core values, and priorities. He and his wife, LeAnne, live in Sandy Springs. His stepdaughter, Madeline, attends college in the northeast.</p>
                    </div>
				</div>
                
             </div>
            </div>
        </div>
        <DccFooter/>
    </Fragment>
);

export default About;
