import React, {Fragment, Component} from 'react';
import { connect } from 'react-redux';
import DccNavBar from './dccNavBar';
import DccFooter from './dccFooter';
import {Link} from 'react-router-dom';
import {get} from 'lodash';
import {fetchMediaIfNeeded, fetchBlogIfNeeded, fetchWorshipSamplesIfNeeded} from '../actions';

class Home extends Component {
    constructor(props) {
        super(props);

        this.state = {
            sermon: false,
            song: false,
            medley: false
        };
    }

    render() {
        let {loggedIn, media, blog, worshipsamples, fetchMediaIfNeeded, fetchBlogIfNeeded, fetchWorshipSamplesIfNeeded} = this.props;

        fetchMediaIfNeeded();
        // fetchBlogIfNeeded();
        // fetchWorshipSamplesIfNeeded();

        return (
            <Fragment>
                <DccNavBar/>
                <div className="page-title home">
                    <div className="page-title-overlay-container">
                        <div className="page-title-overlay"></div>
                    </div>
                    <div className="page-title-text-container">
                        <h1 className="text-center page-title-text">SUNDAY SERVICES ONLINE AND IN PERSON<br/><a target="_blank" href="/events">Register to attend a service</a><br/><a target="_blank" href="https://www.youtube.com/channel/UCn8bqjJ0oA9N0cRDSmLYg5w">Watch on YouTube</a></h1>
                    </div>
                </div>
                <div class='center-info'>
                <p>During the present situation, we are holding much smaller services on Sunday mornings, one service from 9:00 to 10:00 AM and a second service from 11:00 to 12:00.  The services are both the same, and we'd love for you to attend.  We ask you to please register online ahead of time so we can control the number of people in the sanctuary.  Register on our <a target="_blank" href="/events">Events page</a>.</p>
                <p>You can always watch our services online at our <a target="_blank" href="https://www.youtube.com/channel/UCn8bqjJ0oA9N0cRDSmLYg5w">YouTube Channel</a>. Or just search YouTube for "Dunwoody Community Church". Services are posted each Sunday morning so you can watch the service at any time.</p>
                
                <p>You can also listen to our sermons, either <a target="_blank" href="/media">here on our website</a>, through <a target="_blank" href="https://podcasts.apple.com/us/podcast/dunwoody-community-church/id1434335396">iTunes</a>, the <a target="_blank" href="https://play.google.com/music/listen?u=0#/ps/Iwigamd26kvyfk4w3es4mwy7h4q">Google Play Store</a>, through <a target="_blank" href="https://open.spotify.com/show/4frsQ5GvAqVJeT3Ewl4eKq">Spotify</a>, or by pointing your favorite Podcast app to <a target="_blank" href="https://dunwoodychurch.org/podcast/sermons.xml">our feed</a>.</p>
                
                <p>If you are not receiving emails from the church and would like to, <a href="mailto:info@dunwoodychurch.org?subject=Mailing List">click here</a> to send us an email <i>(info@dunwoodychurch.org)</i>, and we will include you on our mailing list.</p>

                <p>Yes, we are in some uncharted territories, but we can be certain that God is in control, and He will continue to lead us forward. As the Psalmist says, "Some trust in chariots, and some trust in horses, but we trust in the name of the LORD our God" (Psalm 20:7).  Don’t fear, don’t be anxious, we will make it through this together.</p>
                
                <p className="text-center page-title-text">Συνευδοκοῦμεν - We Expect Good Together!</p>

				<hr/>
				</div>
                <div className="split-pane-container">
                    <div className="split-pane-left">
                        <div>
							<h2 className="text-center">WHAT TO EXPECT</h2>
                            <hr/>
                            <div className="center-content">
								<img srcSet="https://dccsiteimages.s3.amazonaws.com/home_expect.jpg" width="100%" /> 
                            </div>
                            <p>
                                Sunday morning at DCC is all about connection. Whether through Scripture, prayer, music, or teaching, our goal is to give you opportunities to engage with God on a personal level. We want to <b>be with Jesus</b> together, so that we <b>become like Jesus</b>, in order to <b>do what Jesus did</b>. At the end of our Sunday morning service, we hope you will leave with a deeper understanding of what it means to be a disciple of Jesus Christ.
                            </p>
                            <p>
                                Come as you are, ready for fellowship and worship. Greet our Connections Team, check in your kids in our Children’s Hall, and grab a cup of coffee from Café 1820 before our service starts at 10:30 AM. We look forward to meeting you!
                            </p>
                        </div>

                       <hr/>
                        <div>
                            <h2 className="text-center">Time and Location</h2>
                            <div className="small-group-container">
                              <div className="map-image-container">
                                <a target="_blank" href="https://www.google.com/maps/place/2250+Dunwoody+Club+Dr,+Atlanta,+GA+30350/"><img width="220px" srcSet="https://dccsiteimages.s3.amazonaws.com/map.jpg"/></a>
                              </div>
                              <div className="small-group-info-container">
                                <p><b>Our regular Sunday Service begins at 10:30 AM.</b><br/>
                                There's a Children's program and a Nursery for the young ones.<br/>
                                A Cry Room is available as well in the back of the sanctuary.</p>
                                <p>We are located at:<br/>
                                   <b>2250 Dunwoody Club Dr.<br/>Atlanta, GA 30350</b></p>
                                <p>Phone: <a href="tel:7703968600">770-396-8600</a><br/>
                                   Email: <a href="mailto:info@dunwoodychurch.org">info@dunwoodychurch.org</a></p>
                                <p>Map: <a target="_blank" href="https://www.google.com/maps/place/2250+Dunwoody+Club+Dr,+Atlanta,+GA+30350/">2250 Dunwoody Club Dr., Atlanta, GA 30350-5408</a></p>
                                <p>The church is open Monday - Friday from 9:00am to 3:00pm. You can leave a message any time on our voice mail, and we will respond to you as soon as possible.</p>
                              </div>
                            </div>
                        </div>
                    </div>
                    <div className="split-pane-right">
                        <h2 className="text-center">WHAT YOU'LL HEAR</h2>
                        <hr/>
                        <div className="home-media-container">
                            <div className="home-media-title-container">
                                <h3 className="text-center">Current Sermon Series: {get(media.data, 'rss.channel.item') && media.data.rss.channel.item.sort((media1, media2) => (new Date(media2.pubDate)).valueOf() - (new Date(media1.pubDate)).valueOf())[0]['itunes:summary']}</h3>
                                <Link to="/media" className="link-button">See All Sermons</Link>
                            </div>
                            {get(media.data, 'rss.channel.item') &&
                                <div className="home-media-content-container with-overlay">
                                    <img srcSet={media.data.rss.channel.item[0]['itunes:image'].href} width="300px"/>
                                    {!this.state.sermon &&
                                        <div className="home-media-overlay-container" onClick={() => this.setState({sermon: true})}>
                                            <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 96 96" width="150px">
                                                <g>
                                                    <polygon id="XMLID_3_" fill="rgba(255, 255, 255, 0.8)" points="20.9,15.5 20.9,80.5 75.1,48  "/>
                                                </g>
                                            </svg>
                                        </div>
                                    }
                                    {this.state.sermon &&
                                        <audio controls autoPlay>
                                            <source src={media.data.rss.channel.item[0].enclosure.url} type={media.data.rss.channel.item[0].enclosure.type} />
                                        </audio>
                                    }
                                </div>
                            }
                        </div>
                        <hr/>
                        <div className="home-media-container">
                            <div className="home-media-title-container">
                                <h3 className="text-center">Worship Samples</h3>
                                <p className="text-center">Listen to a medley of songs as our worship teams lead the congregation in worship through singing.</p>
                            </div>
                            <div className="home-media-content-container with-overlay">
                                <img srcSet="https://dccsiteimages.s3.amazonaws.com/home_hear_worship.jpg" width="300px"/>
                                {!this.state.medley &&
                                    <div className="home-media-overlay-container" onClick={() => this.setState({medley: true})}>
                                        <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 96 96" width="150px">
                                            <g>
                                                <polygon id="XMLID_3_" fill="rgba(255, 255, 255, 0.8)" points="20.9,15.5 20.9,80.5 75.1,48  "/>
                                            </g>
                                        </svg>
                                    </div>
                                }
                                {this.state.medley &&
                                    <audio controls autoPlay>
                                        <source src="https://dccsiteimages.s3.amazonaws.com/files/song_medley.mp3" type="audio/mp3" />
                                    </audio>
                                }
                            </div>

                            {/* get(worshipsamples.data, 'data[0].id') &&
                                <div className="home-media-content-container with-overlay">
                                    <img srcSet="https://dccsiteimages.s3.amazonaws.com/home_hear_worship.jpg" width="300px"/>
                                    {!this.state.worshipsample &&
                                        <div className="home-media-overlay-container" onClick={() => this.activatePlayer(worshipsamples.data.data[0].id)}>
                                            <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 96 96" width="150px">
                                                <g>
                                                    <polygon id="XMLID_3_" fill="rgba(255, 255, 255, 0.9)" points="20.9,15.5 20.9,80.5 75.1,48  " />
                                                </g>
                                            </svg>
                                        </div>
                                    }
                                    {this.state.worshipsample &&
                                        <audio controls autoPlay>
                                            <source src={this.state.worshipsample} type={worshipsamples.data.data[0].attributes.content_type} />
                                        </audio>
                                    }
                                </div>
                            */}
                        </div>
                        {/* <hr/>
                        <div className="home-media-container">
                            <div className="home-media-title-container">
                                <h3 className="text-center">Celebrate the Resurrection</h3>
                                <p className="text-center">Listen (and maybe sing along!) with our worship team from Easter Sunday.</p>
                            </div>
                            <div className="home-media-content-container with-overlay">
                                <img srcSet="https://dccsiteimages.s3.amazonaws.com/nograve.jpg" width="300px"/>
                                {!this.state.song &&
                                    <div className="home-media-overlay-container" onClick={() => this.setState({song: true})}>
                                        <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 96 96" width="150px">
                                            <g>
                                                <polygon id="XMLID_3_" fill="rgba(255, 255, 255, 0.8)" points="20.9,15.5 20.9,80.5 75.1,48  "/>
                                            </g>
                                        </svg>
                                    </div>
                                }
                                {this.state.song &&
                                    <audio controls autoPlay>
                                        <source src="https://dccsiteimages.s3.amazonaws.com/files/aint_no_grave.mp3" type="audio/mp3" />
                                    </audio>
                                }
                            </div>
                        </div> */}
                        <hr/>
                        <div>
                           <h2 className="text-center">Read the Bible with us</h2>
                            <p>Absolutely nothing is more impactful for your Spiritual life than reading the Bible.  Reading your Bible regularly is the best way to know and enjoy God.  Many of us at DCC are reading through the Bible this year using a Bible Reading Plan.</p>
                            <p><ul>
								<li>The <a target="_blank" href="https://s3.amazonaws.com/dccresources/F260-Reading-Plan.pdf">F260 Plan</a> reads (on average) two chapters a day, five days a week.  You won't read the entire Bible, but you will read most of it.</li>
								<li>The <a target="_blank" href="https://s3.amazonaws.com/dccresources/BibleReadingPlan.pdf">shorter Mcheyne Plan</a> reads three chapters a day, 365 days a year, and you'll read the whole Bible through in a year.</li>
								<li>The <a target="_blank" href="https://s3.amazonaws.com/dccresources/mcheyne_bookmarks.pdf">longer Mcheyne plan</a> reads four chapters a day, 365 days a year, and you'll read the entire Bible once and the New Testament and the Psalms twice each year.</li>							
							</ul></p>
							<p>Print out the documents above, double-sided, and you have a booklet to guide your reading.</p>
                            <p>You can find dozens of different reading plans <a target="_blank" href="https://genewhitehead.com/bible-reading-plans/">online</a> and through Bible apps like <a target="_blank" href="https://www.youversion.com/">YouVersion</a>.  Whatever plan you use, the most important thing is that you use it!  So join us this year in making God's word a part of each day.  You'll never regret it.</p>
                           
                        </div>
                        <div>
                           <h2 className="text-center">Pray with us</h2>
                            <p>Martin Luther said, "To be a Christian without prayer is no more possible than to be alive without breathing."  But sadly, it's easy to live most of our Christian lives without speaking or listening to God.  Prayer is hard!  Here's some of the prayer exercises that we use at DCC to help us to pray together on Sunday mornings.</p>
                            <p><ul>
								<li><a target="_blank" href="https://s3.amazonaws.com/dccresources/daily-examen.pdf">The Daily Examen</a> is a prayer exercise from St. Ignatius of Loyola.  He taught his monks to do this short exercise twice each day to re-orient them towards Christ.</li>
								<li><a target="_blank" href="https://s3.amazonaws.com/dccresources/ACTS.pdf">ACTS</a> is the classic prayer exercise which uses the acronym "A.C.T.S." to remind us that prayer is more, but not less, than asking God for things - Adoration, Confession, Thanksgiving, and Supplication.</li>
								<li><a target="_blank" href="https://s3.amazonaws.com/dccresources/hexagon.pdf">The Hexagon</a> takes us through the Lord's Prayer to help us focus on God and our need for him.</li>							
							</ul></p>
							<p>Print out the documents above, cut them in half, and put copies in places to remind you to stop and consult with God during your day.  You'll be glad that you did.</p> 
                        </div>
                        <hr/>
                    {/* <div className="home-media-container">
                            <div className="home-media-title-container">
                                <h3 className="text-center">{get(blog.data, 'rss.channel.title') && blog.data.rss.channel.title}</h3>
                                <a className="link-button" target="_blank" href="https://dunwoodyvoices.org">Read Our Blog</a>
                            </div>
                            {get(blog.data, 'rss.channel.title') &&
                                <div className="home-media-content-container text">
                                    <div>
                                        <div className="text-center"><h4>{`${moment(blog.data.rss.channel.item[0].pubDate).format('MMMM D')} - ${blog.data.rss.channel.item[0].title}`} <span className="badge">{blog.data.rss.channel.item[0]['slash:comments']}</span></h4></div>
                                        <div dangerouslySetInnerHTML={{__html: `${blog.data.rss.channel.item[0].description}`}}></div>
                                    </div>
                                </div>
                            }
                        </div> */}
                    </div>
                </div>
                <DccFooter/>
            </Fragment>
        );
    }

    activatePlayer(attachmentId) {
        fetch('/api/attachment', {
            method: 'POST',
            body: JSON.stringify({mediaId: "2241204", attachmentId}),
            headers: {
                'Content-Type': 'application/json'
            },
            credentials: 'include'
        }).then(response => {
            return response.json();
        }).then(data => {
            this.setState({worshipsample: data.data.attributes.attachment_url});
        }).catch(err => {
            console.log('parsing failed', err)
        });
    }
};

const mapStateToProps = (state) => {
    return {
        loggedIn: state.loggedIn,
        media: state.media,
        blog: state.blog,
        worshipsamples: state.worshipsamples
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchMediaIfNeeded: () => dispatch(fetchMediaIfNeeded()),
        fetchBlogIfNeeded: () => dispatch(fetchBlogIfNeeded()),
        fetchWorshipSamplesIfNeeded: () => dispatch(fetchWorshipSamplesIfNeeded()),
    };
}

const HomeLayout = connect(
    mapStateToProps,
    mapDispatchToProps
)(Home);

export default HomeLayout;
