import React, {Fragment, Component} from 'react';
import { connect } from 'react-redux';
import {groupBy} from 'lodash';
import DccNavBar from './dccNavBar';
import DccFooter from './dccFooter';
import Login from './login';
import {Link} from 'react-router-dom';
import {fetchCurriculumsIfNeeded, fetchPlansIfNeeded, login} from '../actions';

let classOrder = {
    'Nursery Teacher': 1,
    'Seedlings 2-3': 2,
    'Dogwoods 4-K': 3,
    "Children's Church Grades 1-5": 4,
}

class Curriculum extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let {loggedIn, curriculums, plans, fetchCurriculumsIfNeeded, fetchPlansIfNeeded, login} = this.props;

        if (loggedIn != 'dccyouth') {
            return (<Login login={login} title={'Teacher Portal'} username={'dccyouth'}/>);
        }

        fetchCurriculumsIfNeeded();
        fetchPlansIfNeeded();

        let selectedCurriculum = {};
        let curriculumLessonsByCurriculumId = {};
        let selectedCurriculumAttachments = [];

        if (curriculums.data) {
            selectedCurriculum = curriculums.data.data.find(curriculum => curriculum.id == this.props.match.params.curriculumId);
            curriculumLessonsByCurriculumId = groupBy(curriculums.data.included, attachment => attachment.relationships.attachable.data.id);
            selectedCurriculumAttachments = curriculumLessonsByCurriculumId[selectedCurriculum.id] || [];
        }

        return (
            <Fragment>
                <DccNavBar toggle={'Menu'}>
                    <Link to="/teacherportal">Schedule</Link>
                    {curriculums.data && curriculums.data.data.sort((curriculum1, curriculum2) => classOrder[curriculum1.attributes.themes] - classOrder[curriculum2.attributes.themes]).map(curriculum =>
                        <Link className={window.location.pathname == `/teacherportal/curriculum/${curriculum.id}` ? 'active' : ''} to={`/teacherportal/curriculum/${curriculum.id}`}>{curriculum.attributes.title}</Link>
                    )}
                    <Link to="/teacherportal/teams">Meet Our Team</Link>
                </DccNavBar>
                <div className="page-title curriculum">
                    <div className="page-title-overlay-container">
                        <div className="page-title-overlay"></div>
                    </div>
                    <div className="page-title-text-container">
                        <h1 className="text-center page-title-text">{selectedCurriculum.attributes && selectedCurriculum.attributes.title}</h1>
                    </div>
                </div>
                {selectedCurriculumAttachments.sort((attachment1, attachment2) => parseInt(attachment1.attributes.filename.match(/\d+/g)) - parseInt(attachment2.attributes.filename.match(/\d+/g))).map(attachment =>
                    <div className="text-center">
                        <a key={attachment.id} className="curriculum-title" onClick={() => this.downloadCurriculumLesson(selectedCurriculum.id, attachment.id, attachment.attributes.filename)}>{attachment.attributes.filename.replace('.pdf', '')}</a>
                    </div>
                )}
                <DccFooter/>
            </Fragment>
        );
    }

    downloadCurriculumLesson(mediaId, attachmentId, fileName) {
        fetch('/api/attachment', {
            method: 'POST',
            body: JSON.stringify({mediaId, attachmentId}),
            headers: {
                'Content-Type': 'application/json'
            },
            credentials: 'include'
        }).then(function (response) {
            return response.json();
        }).then(function (data) {
            let link = document.createElement('a');
            link.download = fileName;
            link.href = data.data.attributes.attachment_url;
            link.style.display = 'none';
            document.body.appendChild(link);
            link.click();
            link.remove();
        }).catch(function (err) {
            console.log('parsing failed', err)
        });
    }
};

const mapStateToProps = (state) => {
    return {
        loggedIn: state.loggedIn,
        curriculums: state.curriculums,
        plans: state.plans
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchCurriculumsIfNeeded: () => dispatch(fetchCurriculumsIfNeeded()),
        fetchPlansIfNeeded: () => dispatch(fetchPlansIfNeeded()),
        login: (user) => dispatch(login(user))
    };
}

const CurriculumLayout = connect(
    mapStateToProps,
    mapDispatchToProps
)(Curriculum);

export default CurriculumLayout;
