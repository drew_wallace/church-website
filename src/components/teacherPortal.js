import React, {Fragment, Component} from 'react';
import { connect } from 'react-redux';
import {groupBy, values, keyBy, get} from 'lodash';
import moment from 'moment';
import DccNavBar from './dccNavBar';
import DccFooter from './dccFooter';
import Login from './login';
import {Link} from 'react-router-dom';
import {fetchCurriculumsIfNeeded, fetchPlansIfNeeded, login} from '../actions';

let classOrder = {
    'Nursery': 1,
    'Toddlers': 2,
    'Preschool/Pre-K': 3,
    'K-1st': 4,
    "Children's Church": 5,
    "Children's Greeter": 6,
    'Preschool/Pre-K Buddy': 7
}

class TeacherPortal extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let {loggedIn, curriculums, plans, fetchCurriculumsIfNeeded, fetchPlansIfNeeded, login} = this.props;

        if (loggedIn != 'dccyouth') {
            return (<Login login={login} title={'Teacher Portal'} username={'dccyouth'}/>);
        }

        fetchCurriculumsIfNeeded();
        fetchPlansIfNeeded();

        let curriculumLessonsByCurriculumId = {};
        let curriculumsByClass = {};

        if (curriculums.data) {
            curriculumLessonsByCurriculumId = groupBy(curriculums.data.included, attachment => attachment.relationships.attachable.data.id);
            curriculumsByClass = keyBy(curriculums.data.data, curriculum => curriculum.attributes.themes);
        }

        const allTeamPositionsFromPlans = get(plans.data, 'data', []).reduce((teamMembers, plan) => {
            plan.team_members.data.forEach((teamMember) => {
                const teamPosition = teamMember.attributes.team_position_name;
                if (!teamMembers.includes(teamPosition)) {
                    teamMembers.push(teamPosition);
                }
            })
            return teamMembers;
        }, []).sort((teamPosition1, teamPosition2) => (classOrder[teamPosition1] || Infinity) - (classOrder[teamPosition2] || Infinity));

        return (
            <Fragment>
                <DccNavBar toggle={'Menu'}>
                    <Link className={window.location.pathname == '/teacherportal' ? 'active' : ''} to="/teacherportal">Schedule</Link>
                    {get(curriculums.data, 'data') && curriculums.data.data.sort((curriculum1, curriculum2) => classOrder[curriculum1.attributes.themes] - classOrder[curriculum2.attributes.themes]).map(curriculum =>
                        <Link to={`/teacherportal/curriculum/${curriculum.id}`}>{curriculum.attributes.title}</Link>
                    )}
                    <Link to="/teacherportal/teams">Meet Our Team</Link>
                </DccNavBar>
                <div className="page-title teacher-portal">
                    <div className="page-title-overlay-container">
                        <div className="page-title-overlay"></div>
                    </div>
                    <div className="page-title-text-container">
                        <h1 className="text-center page-title-text small">Welcome to the DCC Children's Ministry Parent & Teacher Portal</h1>
                        <br/>
                        <div className="page-title-button-container">
                            <a className="link-button fill" href="mailto:jyoung@dunwoodychurch.org">QUESTIONS? CLICK HERE!</a>
                        </div>
                    </div>
                </div>
                <h2 className="text-center">TEACHING SCHEDULE</h2>
                <table className="responsive-table">
                    <thead>
                        <tr>
                            <td>Date</td>
                            {allTeamPositionsFromPlans.map((teamPosition) => (<td>{teamPosition}</td>))}
                        </tr>
                    </thead>
                    <tbody>
                        {get(plans.data, 'data') && get(curriculums.data, 'data') && plans.data.data.map(plan => {
                            let teamMembersList = values(groupBy(plan.team_members.data, teamMember => teamMember.attributes.team_position_name));

                            return (
                                <tr key={plan.id}>
                                    <td data-label="Date">
                                        {moment(plan.attributes.sort_date).format('MMMM D')}
                                    </td>
                                    {allTeamPositionsFromPlans.map((teamName, index) => {
                                        const teamMembers = teamMembersList.find((teamMembers) => teamMembers[0].attributes.team_position_name === teamName);

                                        if(!teamMembers) {
                                            return (<td key={plan.id + index} data-label={teamName}><div className="teacher-container"></div></td>);
                                        }

                                        const curriculumAttachments = curriculumLessonsByCurriculumId[(curriculumsByClass[teamName] || {}).id] || [];
                                        const teamAttachments = plan.attachments.data.filter(attachment => curriculumAttachments.find(curriculumAttachment => curriculumAttachment.attributes.filename == attachment.attributes.filename));

                                        return (
                                            <td key={plan.id + index} data-label={teamName}>
                                                <div className="teacher-container">
                                                    {teamMembers.map(teamMember =>
                                                        <div key={teamMember.id}>{teamMember.attributes.name}</div>
                                                    )}
                                                    {teamAttachments.map(teamAttachment =>
                                                        <div key={teamAttachment.attributes.filename}>
                                                            <a className="lesson-title" onClick={() => this.downloadLesson(plan.id, teamAttachment.id, teamAttachment.attributes.filename)}>
                                                                {teamAttachment.attributes.filename.replace('.pdf', '').replace('.docx', '')}
                                                            </a>
                                                        </div>
                                                    )}
                                                </div>
                                            </td>
                                        );
                                    })}
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
                <DccFooter/>
            </Fragment>
        );
    }

    downloadLesson(planId, attachmentId, fileName) {
        fetch('/api/plans/attachment', {
            method: 'POST',
            body: JSON.stringify({planId, attachmentId}),
            headers: {
                'Content-Type': 'application/json'
            },
            credentials: 'include'
        }).then(function (response) {
            return response.json();
        }).then(function (data) {
            let link = document.createElement('a');
            link.download = fileName;
            link.href = data.data.attributes.attachment_url;
            link.style.display = 'none';
            document.body.appendChild(link);
            link.click();
            link.remove();
        }).catch(function (err) {
            console.log('parsing failed', err)
        });
    }
};

const mapStateToProps = (state) => {
    return {
        loggedIn: state.loggedIn,
        curriculums: state.curriculums,
        plans: state.plans
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchCurriculumsIfNeeded: () => dispatch(fetchCurriculumsIfNeeded()),
        fetchPlansIfNeeded: () => dispatch(fetchPlansIfNeeded()),
        login: (user) => dispatch(login(user))
    };
}

const TeacherPortalLayout = connect(
    mapStateToProps,
    mapDispatchToProps
)(TeacherPortal);

export default TeacherPortalLayout;
