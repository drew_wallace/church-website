import React, {Fragment, Component} from 'react';
import DccNavBar from './dccNavBar';
import DccFooter from './dccFooter';
import {ToastContainer, toast} from 'react-toastify';
import checkStatus from '../lib/checkStatus';

class Login extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        this.checkIfLoggedIn();

        return (
            <Fragment>
                <ToastContainer />
                <DccNavBar/>
                <div className="login-container">
                    <h4 className="text-center">You need a password to view the {this.props.title}:</h4>
                    <div><input autoFocus ref="password" type="password" name="password" placeholder="Password" onKeyPress={e => e.key == 'Enter' && this.login()}/></div>
                    <button className="link-button primary" onClick={() => this.login()}>Login</button>
                </div>
                <DccFooter/>
            </Fragment>
        );
    }

    checkIfLoggedIn() {
        fetch('/login', {
            headers: {
                'Content-Type': 'application/json'
            },
            credentials: 'include'
        }).then(response => {
            return response.json();
        }).then(data => {
            if(data.user) {
                this.props.login(data.user);
            }
        }).catch(function (err) {
            console.error('parsing failed', err)
        });
    }

    login() {
        fetch('/login', {
            method: 'POST',
            body: JSON.stringify({username: this.props.username, password: this.refs.password.value}),
            headers: {
                'Content-Type': 'application/json'
            },
            credentials: 'include'
        }).then(checkStatus)
        .then(response => response.json())
        .then(data => {
            this.props.login(data.user);
        }).catch(async (err) => {
            const response = await err.response.json();
            toast.error(response.message, {
                position: 'top-center',
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                draggablePercent: 60
            });
            console.error(response);
        });
    }
};

export default Login;