import React, {Fragment, Component} from 'react';
import { connect } from 'react-redux';
import DccNavBar from './dccNavBar';
import DccFooter from './dccFooter';

const Give = () => (
    <Fragment>
        <DccNavBar/>
        <div className="page-title give">
            <div className="page-title-overlay-container">
                <div className="page-title-overlay"></div>
            </div>
            <div className="page-title-text-container">
                <h1 className="text-center page-title-text">GIVING</h1>
            </div>
        </div>
        <div>
            <div className="split-pane-container">
                <div className="split-pane-left">
                    <h2 className="text-center">How To Give</h2>
                    <p>Our ministry is financially supported by the people of our church. We take a formal offering the last Sunday of each month.  In addition, you can drop your financial gift in one of the offering boxes located throughout the building, or you can make a secure online donation through the button below. </p>
                    <div className="text-center margin-top">
                        <a className="link-button big" target="_blank" href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=GW53KUFJN9AYN">GIVE ONLINE</a>
                    </div>
                </div>
                <div className="split-pane-right">
                    <div className="text-center amazon"> <img src="https://dccsiteimages.s3.amazonaws.com/amazon_smiles.png" alt="Amazon Smiles"></img><p className="amazon">Amazon Smile is an initiative by on-line retailer Amazon.com that donates 0.5% of every purchase to the charity of your choice.  <a target="_blank" href="https://smile.amazon.com/ch/58-1303244">Click HERE to select Dunwoody Community Church as your charity of choice.</a>  Then whenever you visit Amazon, log-on at <a target="_blank" href="https://smile.amazon.com">smile.amazon.com</a> rather than <i>www.amazon.com</i> and half a percent of your purchases will be donated to the church.</p>
                    </div>
                </div>
            </div>
        </div>
        <DccFooter/>
    </Fragment>
);

export default Give;
