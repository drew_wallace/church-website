import React, {Fragment, Component} from 'react';
import { connect } from 'react-redux';
import {uniq, get} from 'lodash';
import moment from 'moment';
import DccNavBar from './dccNavBar';
import DccFooter from './dccFooter';
import {fetchMediaIfNeeded} from '../actions';

class Media extends Component {
    constructor(props) {
        super(props);

        this.state = {
            per_page: 9,
            offset: 0,
            series: '',
            speaker: '',
            audio: {}
        }
    }

    render() {
        let {loggedIn, media, fetchMediaIfNeeded} = this.props;

        fetchMediaIfNeeded();

        let series = [];
        let speakers = [];
        let pagination = null;

        if(get(media.data, 'rss.channel.item')) {
            series = uniq(media.data.rss.channel.item.filter(media => media['itunes:summary'] && (this.state.speaker ? media['itunes:author'] == this.state.speaker : true)).map(media => media['itunes:summary']));
            speakers = uniq(media.data.rss.channel.item.filter(media => media['itunes:author'] && (this.state.series ? media['itunes:summary'] == this.state.series : true)).map(media => media['itunes:author']));
            pagination = (
                <div className="center-content pad wrap">
                    <label htmlFor="per_page" className="pagination-label">Per page:</label>
                    <input type="number" min={1} max={100} className="pagination-page" onChange={event => this.setState({per_page: event.target.value ? Math.max(Math.min(parseInt(event.target.value), 100), 1) : '', audio: {}})} value={this.state.per_page} />
                    {this.state.per_page &&
                        <div className="pagination-buttons-container">
                            {Array.from({length: Math.ceil(media.data.rss.channel.item.sort((media1, media2) => (new Date(media2.pubDate)).valueOf() - (new Date(media1.pubDate)).valueOf()).filter(media => this.state.series ? media['itunes:summary'] == this.state.series : true).filter(media => this.state.speaker ? media['itunes:author'] == this.state.speaker : true).length / this.state.per_page)}, () => 0).map((ignoreThis, index) =>
                                <button key={index} className="pagination-button" style={index == Math.ceil(this.state.offset / this.state.per_page) ? {background: '#337ab7', color: 'white'} : {}} onClick={() => this.setState({offset: index * this.state.per_page, audio: {}})}>{index + 1}</button>
                            )}
                        </div>
                    }
                </div>
            );
        }

        return (
            <Fragment>
                <DccNavBar/>
                <div className="page-title media">
                    <div className="page-title-overlay-container">
                        <div className="page-title-overlay"></div>
                    </div>
                    <div className="page-title-text-container">
                        <h1 className="text-center page-title-text">MEDIA</h1>
                        <div className="page-title-button-container">
                            <a target="_blank" href="https://www.youtube.com/channel/UCn8bqjJ0oA9N0cRDSmLYg5w"><img src="https://dccsiteimages.s3.amazonaws.com/youtube.png" width="120" alt="Youtube" /></a>
						</div>
						<div className="page-title-button-container">
                            <a target="_blank" href="https://open.spotify.com/show/4frsQ5GvAqVJeT3Ewl4eKq"><img src="https://dccsiteimages.s3.amazonaws.com/spotify.png" width="120" alt="Spotify" /></a>
						</div>
                        <div className="page-title-button-container">
                            <a target="_blank" href="https://play.google.com/music/listen?u=0#/ps/Iwigamd26kvyfk4w3es4mwy7h4q"><img src="https://dccsiteimages.s3.amazonaws.com/play-badge.png" width="120" alt="Google Play" /></a>
						</div>
                        <div className="page-title-button-container">
                            <a target="_blank" href="https://itunes.apple.com/us/podcast/dunwoody-community-church/id1434335396"><img src="https://dccsiteimages.s3.amazonaws.com/itunes-badge.png" width="120" alt="iTunes" /></a>
						</div>
                        <div className="page-title-button-container">
                            <a target="_blank" href="https://dunwoodychurch.org/podcast/sermons.xml"><img src="https://dccsiteimages.s3.amazonaws.com/rss-badge.png" width="120" alt="rss feed" /></a>
                        </div>
                    </div>
                </div>
                <div className="center-content wrap">
                    <div>
                        <select className="media-filter" value={this.state.series} onChange={e => this.setState({series: e.target.value, audio: {}, offset: 0})}>
                            <option value="">Any Series</option>
                            {series.map(seriesName =>
                                <option value={seriesName}>{seriesName}</option>
                            )}
                        </select>
                    </div>
                    <div>
                        <select className="media-filter" value={this.state.speaker} onChange={e => this.setState({speaker: e.target.value, audio: {}, offset: 0})}>
                            <option value="">Any Speaker</option>
                            {speakers.map(speakerName =>
                                <option value={speakerName}>{speakerName}</option>
                            )}
                        </select>
                    </div>
                </div>
                {pagination}
                <div className="collection-pane">
                    {get(media.data, 'rss.channel.item') && media.data.rss.channel.item.sort((media1, media2) => (new Date(media2.pubDate)).valueOf() - (new Date(media1.pubDate)).valueOf()).filter(media => this.state.series ? media['itunes:summary'] == this.state.series : true).filter(media => this.state.speaker ? media['itunes:author'] == this.state.speaker : true).filter((media, index) => index >= this.state.offset && index < this.state.offset + this.state.per_page).map(media =>
                        <div className="collection-item-3">
                            <div className="media-container">
                                <div className="media-image-container">
                                    <img srcSet={media['itunes:image'].href} width="300px" />
                                    {!this.state.audio[media.guid] &&
                                        <div className="media-image-button-container" onClick={() => this.activatePlayer(media.guid)}>
                                            <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 96 96" width="150px">
                                                <g>
                                                    <polygon id="XMLID_3_" fill="rgba(255, 255, 255, 0.9)" points="20.9,15.5 20.9,80.5 75.1,48  " />
                                                </g>
                                            </svg>
                                        </div>
                                    }
                                </div>
                                {this.state.audio[media.guid] &&
                                    <audio controls autoPlay>
                                        <source src={media.enclosure.url} type={media.enclosure.type} />
                                    </audio>
                                }
                                <div className="media-description-container">
                                    <div><b>{media.title}</b></div>
                                    <div>Date: {moment(media.pubDate).format('ddd, MMM D, YYYY')}</div>
                                    {media['itunes:summary'] && <div>Series: {media['itunes:summary']}</div>}
                                    <div>Speaker: {media['itunes:author']}</div>
                                    <div>Length: {media['itunes:duration']}</div>
                                    { media['videoLink'] &&
                                    <div><a target="_blank" href={media['videoLink']}>Watch on Youtube</a></div>
									}
                                </div>
                            </div>
                        </div>
                    )}
                </div>
                {pagination}
                <DccFooter/>
            </Fragment>
        );
    }

    activatePlayer(mediaId) {
        this.state.audio[mediaId] = true;
        this.setState({audio: this.state.audio});
    }
};

const mapStateToProps = (state) => {
    return {
        loggedIn: state.loggedIn,
        media: state.media
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchMediaIfNeeded: (options) => dispatch(fetchMediaIfNeeded(options))
    };
}

const MediaLayout = connect(
    mapStateToProps,
    mapDispatchToProps
)(Media);

export default MediaLayout;
