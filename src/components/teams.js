import React, {Fragment, Component} from 'react';
import { connect } from 'react-redux';
import {groupBy, values, mapValues, keyBy, get, sortBy} from 'lodash';
import moment from 'moment';
import DccNavBar from './dccNavBar';
import DccFooter from './dccFooter';
import Login from './login';
import {Link} from 'react-router-dom';
import {fetchCurriculumsIfNeeded, fetchTeamsIfNeeded, login} from '../actions';

let classOrder = {
    'Nursery Teacher': 1,
    'Seedlings 2-3': 2,
    'Dogwoods 4-K': 3,
    "Children's Church Grades 1-5": 4,
}

class Teams extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        let {loggedIn, teams, curriculums, fetchCurriculumsIfNeeded, fetchTeamsIfNeeded, login} = this.props;

        if (loggedIn != 'dccyouth') {
            return (<Login login={login} title={'Teacher Portal'} username={'dccyouth'}/>);
        }

        fetchTeamsIfNeeded();
        fetchCurriculumsIfNeeded();

        let teamsInfo = {};
        if (teams.data) {
            teamsInfo = mapValues(groupBy(teams.data.included, 'type'), groupType => keyBy(groupType, 'id'));
            for(let assignment of values(teamsInfo.PersonTeamPositionAssignment)) {
                let person = teamsInfo.Person[assignment.relationships.person.data.id];
                let teamPosition = teamsInfo.TeamPosition[assignment.relationships.team_position.data.id];
                person.attributes.team_name = teamPosition.attributes.name;
            }
        }

        return (
            <Fragment>
                <DccNavBar toggle={'Menu'}>
                    <Link to="/teacherportal">Schedule</Link>
                    {get(curriculums.data, 'data') && curriculums.data.data.sort((curriculum1, curriculum2) => classOrder[curriculum1.attributes.themes] - classOrder[curriculum2.attributes.themes]).map(curriculum =>
                        <Link to={`/teacherportal/curriculum/${curriculum.id}`}>{curriculum.attributes.title}</Link>
                    )}
                    <Link className={window.location.pathname == '/teacherportal/teams' ? 'active' : ''} to="/teacherportal/teams">Meet Our Team</Link>
                </DccNavBar>
                <div className="page-title teams">
                    <div className="page-title-overlay-container">
                        <div className="page-title-overlay"></div>
                    </div>
                    <div className="page-title-text-container">
                        <h1 className="text-center page-title-text">MEET OUR CHILDREN'S MINISTRY TEAM</h1>
                    </div>
                </div>
                <div className="collection-pane">
                    {sortBy(values(teamsInfo.Person), person => person.attributes.last_name).map(person =>
                        <div className="collection-item-3">
                            <div>
                                {`${person.attributes.first_name} ${person.attributes.last_name}`}
                            </div>
                            <div>
                                <img srcSet={person.attributes.photo_thumbnail_url} className="person-image" />
                            </div>
                            <div>
                                {person.attributes.team_name}
                            </div>
                        </div>
                    )}
                </div>
                <DccFooter/>
            </Fragment>
        );
    }
};

const mapStateToProps = (state) => {
    return {
        loggedIn: state.loggedIn,
        curriculums: state.curriculums,
        teams: state.teams
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchTeamsIfNeeded: () => dispatch(fetchTeamsIfNeeded()),
        fetchCurriculumsIfNeeded: () => dispatch(fetchCurriculumsIfNeeded()),
        login: (user) => dispatch(login(user))
    };
}

const TeamsLayout = connect(
    mapStateToProps,
    mapDispatchToProps
)(Teams);

export default TeamsLayout;
