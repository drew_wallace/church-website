import React, {Fragment, Component} from 'react';
import { connect } from 'react-redux';
import DccNavBar from './dccNavBar';
import DccFooter from './dccFooter';

const GetInvolved = () => (
    <Fragment>
        <DccNavBar/>
        <div className="page-title get-involved">
            <div className="page-title-overlay-container">
                <div className="page-title-overlay"></div>
            </div>
            <div className="page-title-text-container">
                <h1 className="text-center page-title-text">GET INVOLVED</h1>
            </div>
        </div>
        <div className="split-pane-container">
            <div className="split-pane-left">
                <h2 className="text-center">Connect with a Community Group</h2>
                <p>Community Groups are the next step in discipleship and connection at DCC.
                Where Sunday mornings offer a chance to be with Jesus, Community Groups encourage us to <b>be like Jesus</b>.
                Community Groups foster authentic community and personal accountability as we walk together through life.
                We have Community Groups meeting all around the Dunwoody/Sandy Springs area and beyond many
                days of the week.  Check out our available community groups below and pray about which one is right for you.</p>
                {/*<p>You can download our <a target="_blank" href="https://dccsiteimages.s3.amazonaws.com/files/smallgroups.pdf">
                Community Groups Brochure</a> to get more information on the groups.  You can also click on the leaders' names
                to email them directly.  Or check out the <a href="/events">Events Page</a> to see exactly when
                different groups meet.</p> */}
                <hr className="dashed"/>

                <div className="small-group-container">
                    <div className="small-group-image-container">
                        <img
                            srcSet="https://dccsiteimages.s3.amazonaws.com/smallgroups/abbott.jpg"/>
                    </div>
                    <div className="small-group-info-container">
                        <h4><a href="mailto:groups@dunwoodychurch.org?subject=Abbott%20group">BETH ABBOTT</a></h4>
                        <div>WHO: Women<br/>WHEN: Weekly Thursday Nights<br/>WHERE: Sandy Springs</div>
                    </div>
                </div>

                <div className="small-group-container">
                    <div className="small-group-image-container">
                        <img
                            srcSet="https://dccsiteimages.s3.amazonaws.com/smallgroups/andrle.jpg"/>
                    </div>
                    <div className="small-group-info-container">
                        <h4><a href="mailto:groups@dunwoodychurch.org?subject=Andre%20group">JOHN & JULIA ANDRLE</a></h4>
                        <div>WHO: Families with small children<br/>WHEN: Monthly Sunday Nights<br/>WHERE: Dunwoody</div>
                    </div>
                </div>

                <div className="small-group-container">
                    <div className="small-group-image-container">
                        <img
                            srcSet="https://dccsiteimages.s3.amazonaws.com/smallgroups/eda.jpg"/>
                    </div>
                    <div className="small-group-info-container">
                        <h4><a href="mailto:groups@dunwoodychurch.org?subject=Beacham%20group">EDA BEACHAM</a></h4>
                        <div>WHO: Women<br/>WHEN: Every other Wednesday Night<br/>WHERE: Dunwoody</div>
                    </div>
                </div>

                <div className="small-group-container">
                    <div className="small-group-image-container">
                        <img
                            srcSet="https://dccsiteimages.s3.amazonaws.com/smallgroups/t-c-beard.jpg"/>
                    </div>
                    <div className="small-group-info-container">
                        <h4><a href="mailto:groups@dunwoodychurch.org?subject=Beard%20group">TIM & CINDY BEARD</a></h4>
                        <div>WHO: Anyone<br/>WHEN: Every other Sunday Night<br/>WHERE: Johns Creek</div>
                    </div>
                </div>

                <div className="small-group-container">
                    <div className="small-group-image-container">
                        <img
                            srcSet="https://dccsiteimages.s3.amazonaws.com/smallgroups/blomgren.jpg"/>
                    </div>
                    <div className="small-group-info-container">
                        <h4><a href="mailto:groups@dunwoodychurch.org?subject=Blomgren%20group">BRIAN & DEBBIE BLOMGREN</a></h4>
                        <div>WHO: Families<br/>WHEN: Every other Saturday Night<br/>WHERE: North Sandy Springs</div>
                    </div>
                </div>

                 <div className="small-group-container">
                    <div className="small-group-image-container">
                            <img
                                srcSet="https://dccsiteimages.s3.amazonaws.com/smallgroups/gilmores.jpg"/>
                    </div>
                    <div className="small-group-info-container">
                        <h4><a href="mailto:groups@dunwoodychurch.org?subject=Gimore%20group">DAVE & LUANN GILMORE</a></h4>
                        <div>WHO: Married Couples<br/>WHEN: Monthly Sunday Nights<br/>WHERE: DCC</div>
                    </div>
                </div>

                <div className="small-group-container">
                    <div className="small-group-image-container">
                        <img
                            srcSet="https://dccsiteimages.s3.amazonaws.com/smallgroups/b-n-hart.jpg"/>
                    </div>
                    <div className="small-group-info-container">
                        <h4><a href="mailto:groups@dunwoodychurch.org?subject=Hart%20group">BOB & NANCY HART</a></h4>
                        <div>WHO: Anyone<br/>WHEN: Every other Sunday Afternoon<br/>WHERE: North Sandy Springs</div>
                    </div>
                </div>

                <div className="small-group-container">
                    <div className="small-group-image-container">
                        <img srcSet="https://dccsiteimages.s3.amazonaws.com/smallgroups/marshall.jpg" />
                    </div>
                    <div className="small-group-info-container">
                        <h4><a href="mailto:groups@dunwoodychurch.org?subject=Marshall%20group">DAVE & KELLEY MARSHALL</a></h4>
                        <div>WHO: Anyone<br/>WHEN: Every other Thursday Night<br/>WHERE: North Sandy Springs</div>
                    </div>
                </div>

                <div className="small-group-container">
                    <div className="small-group-image-container">
                        <img
                            srcSet="https://dccsiteimages.s3.amazonaws.com/smallgroups/m-l-martin.jpg"/>
                    </div>
                    <div className="small-group-info-container">
                        <h4><a href="mailto:groups@dunwoodychurch.org?subject=Martin%20group">MART & LEANNE MARTIN</a></h4>
                        <div>WHO: Anyone<br/>WHEN: Every other Thursday Night<br/>WHERE: Northridge - Sandy Springs</div>
                    </div>
                </div>

                <div className="small-group-container">
                    <div className="small-group-image-container">
                        <img
                            srcSet="https://dccsiteimages.s3.amazonaws.com/smallgroups/stewart.jpg"/>
                    </div>
                    <div className="small-group-info-container">
                        <h4><a href="mailto:groups@dunwoodychurch.org?subject=Stewart%20group">CHIP & SANDRA STEWART</a></h4>
                        <div>WHO: Anyone<br/>WHEN: Every other Sunday Night<br/>WHERE: Dunwoody</div>
                    </div>
                </div>

                <div className="small-group-container">
                    <div className="small-group-image-container">
                            <img
                                srcSet="https://dccsiteimages.s3.amazonaws.com/smallgroups/youngs.jpg"/>
                    </div>
                    <div className="small-group-info-container">
                        <h4><a href="mailto:groups@dunwoodychurch.org?subject=Young%20Adult%20group">JEREMY & JORDAN YOUNG</a></h4>
                        <div>WHO: Young Adults<br/>WHEN: Weekly Thursday Nights<br/>WHERE: DCC</div>
                    </div>
                </div>

              </div>
            <div className="split-pane-right">
                <h2 className="text-center">Serve on a Ministry Team</h2>
                <p>Ministry Teams give disciples of Jesus an opportunity to do what Jesus did here at DCC. Whether they are engaging in practical service or spiritual leadership, our volunteers are the hands and feet of Jesus to those who walk through our doors on Sunday morning. Click on the links below to email us and find out more about engaging in sacrificial service here at DCC.</p>
                <div className="split-pane-container">
                    <div className="split-pane-simple">
                        <h5 className="text-center">Spiritual Leadership</h5>
                        <ul>
                            <li><a href="mailto:jshetler@dunwoodychurch.org">Children's Teacher</a></li>
                            <li><a href="mailto:jshetler@dunwoodychurch.org">Youth Ministry Mentor</a></li>
                            <li><a href="mailto:tbeard@dunwoodychurch.org">Worship Team</a></li>
                            <li><a href="mailto:jjansen@dunwoodychurch.org">Small Group Leader</a></li>
                            <li><a href="mailto:jjansen@dunwoodychurch.org">Connections (Greeters) Team</a></li>
                        </ul>
                    </div>
                    <div className="split-pane-simple">
                        <h5 className="text-center">Practical Service</h5>
                        <ul>
                            <li><a href="mailto:jjansen@dunwoodychurch.org">Audio/Visual</a></li>
                            <li><a href="mailto:info@dunwoodychurch.org">Cleaning</a></li>
                            <li><a href="mailto:info@dunwoodychurch.org">Landscaping</a></li>
                            <li><a href="mailto:tbeard@dunwoodychurch.org">Have a skill you’d like to use at DCC? Let us know about it!</a></li>
                        </ul>
                    </div>
                </div>
                <hr/>
                <div>
                    <h2 className="text-center">Volunteer with Our Partners</h2>
                    <p>
                        Disciples of Christ have a missional mindset; they look outwards and seek to do what Jesus did everywhere they go. Check out our partner organizations for ways to engage in our community for the Kingdom.
                    </p>
                    <ul>
                        <li><a target="_blank" href="https://hopenowusa.org/">Hope Now</a></li>
                        <li><a target="_blank" href="https://ccmatlanta.org/volunteer/">Cross-Cultural Ministries</a></li>
                        <li><a target="_blank" href="http://www.ourcac.org/volunteer/">Community Action Center</a></li>
                        <li><a target="_blank" href="http://www.familypromisenfd.org/">Family Promise</a></li>
                    </ul>
                </div>
                <hr/>
                <div>
                    <h2 className="text-center">Pray with our Church Family</h2>
                    <p>Prayer is the most important thing we do at DCC. We have teams who regularly meet to thank our Father, acknowledge His Lordship, and ask Him to provide for our needs. They would love to have you join them!</p>
                    <table id="prayerTable"><tbody>
                        <tr>
                            <td>Sundays at 9:30 AM<br/>in the Resource Room<br/><i>Everyone Welcome</i></td>
                      {/*     <td>Mondays at 5:00 PM<br/>in the Conference Room<br/><i>Everyone Welcome</i></td>   */}
                            <td>Tuesdays at 10:00 AM<br/>in Café 1820<br/><i>Women's Group</i></td>
                        </tr>
                    </tbody></table>
                    <p>Before you come, please call (<a href="tel:7703968600">770-396-8600</a>) or <a href="mailto:info@dunwoodychurch.org">email</a> the church office to confirm that the prayer meeting is scheduled for that day.</p>

                </div>
                <hr/>
                <div>
                    <h2 className="text-center">Check Out our Missionaries</h2>
                    <p>These are the missionaries whom our church supports corporately by giving, praying, and going.  Check out the links to their ministries to learn more about what they are doing for the kingdom and why we are behind them.</p>
                    <div className="split-pane-container">
                      <div className="split-pane-simple">
                        <h5 className="text-center">Local and National</h5>
                        <ul>
                           <li>Cliff and Judy Anderson - Waxhaw, NC<br/><a target="_blank" href="https://jaars.org/">JAARS</a> (Wycliffe Bible Translators)</li>
                            <li>Virginia Cosgrove - Atlanta, GA<br/><a target="_blank" href="https://ccmatlanta.org/">Cross-Cultural Ministries</a></li>
                           <li>Paul Dzubinski - Los Angeles, CA<br/><a target="_blank" href="https://www.frontierventures.org/">Frontier Ventures</a></li>
                         </ul>
                      </div>
                      <div className="split-pane-simple">
                        <h5 className="text-center">International</h5>
                        <ul>
                            <li>Cyndee Knight - Ukraine<br/><a target="_blank" href="https://hopenowusa.org/">Hope Now</a></li>
                            {/* <li>Shawn and Amy Schantz - South Asia<br/><a target="_blank" href="https://wycliffe.org/">Wycliffe Bible Translators</a></li> */}
                            <li>Joseph and Esther Tan - East Asia<br/><a target="_blank" href="https://afci.us/">Ambassadors for Christ</a></li>
                        </ul>
                      </div>
                    </div>
                </div>

                <div>
                    <hr/>
                    <div className="center-content">
                        <img srcSet="https://dccsiteimages.s3.amazonaws.com/dcs_logo.png" className="noborder" width="100%" />
                    </div>
                    <p>Located on the campus of Dunwoody Community Church, <a target="_blank" href="http://dunwoodycs.org">Dunwoody Christian School</a> is a recently established  Christian elemtary school here in the Dunwoody area.  Their mission is to partner with parents to develop children who are grounded in Christ-like character, possess exceptional knowledge, and are prepared to embrace life’s opportunities and challenges. They are presently still accepting applications for K, 1<sup>st</sup>, 2<sup>nd</sup>, and 3<sup>rd</sup> grades.  <a target="_blank" href="http://dunwoodycs.org">See their web site</a> for more information or to apply.
                    </p>
                </div>

            </div>
        </div>
        <DccFooter/>
    </Fragment>
);

export default GetInvolved;
