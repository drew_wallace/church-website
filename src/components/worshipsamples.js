import React, {Fragment, Component} from 'react';
import { connect } from 'react-redux';
import {uniq} from 'lodash';
import moment from 'moment';
import DccNavBar from './dccNavBar';
import DccFooter from './dccFooter';
import {fetchWorshipSamplesIfNeeded} from '../actions';

class WorshipSamples extends Component {
    constructor(props) {
        super(props);

        this.state = {
            artist: '',
            title: '',
            audio: {}
        }
    }

    render() {
        let {loggedIn, worshipsamples, fetchWorshipSamplesIfNeeded} = this.props;

        fetchWorshipSamplesIfNeeded();

        let artists = [];
        let titles = [];

        console.log(this.state);

        if(worshipsamples.data) {
            worshipsamples.data.data = worshipsamples.data.data.map(worshipsample => {
                let split = worshipsample.attributes.filename.split(' - ');

                worshipsample.attributes.artist = split[0];
                worshipsample.attributes.title = split[1];
                worshipsample.attributes.date = moment(split[2], 'YYYYMMDD').toDate();

                return worshipsample;
            });

            artists = uniq(worshipsamples.data.data.filter(worshipsample => worshipsample.attributes.artist && (this.state.title ? worshipsample.attributes.title == this.state.title : true)).map(worshipsample => worshipsample.attributes.artist));
            titles = uniq(worshipsamples.data.data.filter(worshipsample => worshipsample.attributes.title && (this.state.artist ? worshipsample.attributes.artist == this.state.artist : true)).map(worshipsample => worshipsample.attributes.title));
        }

        return (
            <Fragment>
                <DccNavBar/>
                <div className="page-title media">
                    <div className="page-title-overlay-container">
                        <div className="page-title-overlay"></div>
                    </div>
                    <div className="page-title-text-container">
                        <h1 className="text-center page-title-text">Worship Samples</h1>
                    </div>
                </div>
                <div className="center-content wrap">
                    <div>
                        <select className="media-filter" value={this.state.artist} onChange={e => this.setState({artist: e.target.value})}>
                            <option value="">Any Artist</option>
                            {artists.map(artistName =>
                                <option value={artistName}>{artistName}</option>
                            )}
                        </select>
                    </div>
                    <div>
                        <select className="media-filter" value={this.state.title} onChange={e => this.setState({title: e.target.value})}>
                            <option value="">Any Song</option>
                            {titles.map(titleName =>
                                <option value={titleName}>{titleName}</option>
                            )}
                        </select>
                    </div>
                </div>
                {worshipsamples.data &&
                    <div className="collection-pane">
                        {worshipsamples.data && worshipsamples.data.data.sort((worshipsample1, worshipsample2) => moment(worshipsample2.attributes.date).valueOf() - moment(worshipsample1.attributes.date).valueOf()).filter(worshipsample => this.state.artist ? worshipsample.attributes.artist == this.state.artist : true).filter(worshipsample => this.state.title ? worshipsample.attributes.title == this.state.title : true).map(worshipsample =>
                            <div className="collection-item-3">
                                <div className="media-container">
                                    <div className="media-image-container">
                                    <img srcSet={worshipsample.attributes.thumbnail_url} width="300px" height="170px" />
                                        {!this.state.audio[worshipsample.id] &&
                                            <div className="media-image-button-container" onClick={() => this.activatePlayer(worshipsample.id)}>
                                                <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 96 96" width="150px">
                                                    <g>
                                                        <polygon id="XMLID_3_" fill="rgba(255, 255, 255, 0.9)" points="20.9,15.5 20.9,80.5 75.1,48  " />
                                                    </g>
                                                </svg>
                                            </div>
                                        }
                                    </div>
                                    {this.state.audio[worshipsample.id] &&
                                        <audio controls autoPlay>
                                            <source src={this.state.audio[worshipsample.id]} type={worshipsample.attributes.content_type} />
                                        </audio>
                                    }
                                    <div className="media-description-container">
                                        <div><b>{worshipsample.attributes.title}</b></div>
                                        <div>Date: {moment(worshipsample.attributes.date).format('MM/DD/YYYY')}</div>
                                        {worshipsample.attributes.artist && <div>Series: {worshipsample.attributes.artist}</div>}
                                        <div>Artist: {worshipsample.attributes.title}</div>
                                    </div>
                                </div>
                            </div>
                        )}
                    </div>
                }
                <DccFooter/>
            </Fragment>
        );
    }

    activatePlayer(attachmentId) {
        fetch('/api/attachment', {
            method: 'POST',
            body: JSON.stringify({mediaId: "2241204", attachmentId}),
            headers: {
                'Content-Type': 'application/json'
            },
            credentials: 'include'
        }).then(response => {
            return response.json();
        }).then(data => {
            this.state.audio[attachmentId] = data.data.attributes.attachment_url;
            this.setState({audio: this.state.audio});
        }).catch(err => {
            console.log('parsing failed', err)
        });
    }
};

const mapStateToProps = (state) => {
    return {
        loggedIn: state.loggedIn,
        worshipsamples: state.worshipsamples
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchWorshipSamplesIfNeeded: (options) => dispatch(fetchWorshipSamplesIfNeeded(options))
    };
}

const WorshipSamplesLayout = connect(
    mapStateToProps,
    mapDispatchToProps
)(WorshipSamples);

export default WorshipSamplesLayout;