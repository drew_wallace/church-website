import React, {Component, Fragment} from 'react';
//~ import {Link} from 'react-router-dom';

class DccNavBar extends Component {
    constructor(props) {
        super(props);

        this.state = {
            open: false
        };

        this.onClickDocument = e => this.burgerToggle(false, e);
    }

    componentDidMount() {
        document.querySelector(':not(.navNarrowMenu):not(.narrowLinks)').addEventListener('click', this.onClickDocument);
    }

    componentWillUnmount() {
        document.querySelector(':not(.navNarrowMenu):not(.narrowLinks)').removeEventListener('click', this.onClickDocument)
    }

    render() {
        const links = this.props.children || (
            <Fragment>  {/*
                <Link className={window.location.pathname == '/' ? 'active' : ''} to="/">Sunday Morning</Link>
                <Link className={window.location.pathname == '/getinvolved' ? 'active' : ''} to="/getinvolved">Get Involved</Link>
                <Link className={window.location.pathname == '/youth' ? 'active' : ''} to="/youth">Children & Youth</Link>
                <Link className={window.location.pathname == '/about' ? 'active' : ''} to="/about">About DCC</Link>
                <Link className={window.location.pathname == '/events' ? 'active' : ''} to="/events">Events</Link>
                <Link className={window.location.pathname == '/give' ? 'active' : ''} to="/give">Give</Link>
            */} </Fragment>
        );

        const menu = this.props.menu || (
            <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 1000 1000" xmlSpace="preserve">
                <g>
                    <path d="M316.3,515.3h367.5v-30.6H316.3V515.3z M316.3,392.8h367.5v-30.6H316.3V392.8z M316.3,637.8h367.5v-30.6H316.3V637.8z" />
                </g>
            </svg>
        );

        return (
            <nav>
                <div className="navWide">
                    <a href='https://dunwoodychurch.org'>
                        <img className="navbar-image" height="50px" srcSet="/public/2021_gray_Logo-01.jpg" />
                    </a>
                    <div className="wideDiv">
                        {links}
                    </div>
                </div>
                <div className="navNarrow">
                    <div className="navNarrowMenu" onClick={() => this.burgerToggle(true)}>{menu}</div>
                    <div className="narrowLinks" style={{display: this.state.open ? 'block' : 'none'}}>
                        {links}
                    </div>
                </div>
            </nav>
        );
    }

    burgerToggle(open, e) {
        // * Checking for typeof object because of the svg icon
        if (e && e.target.className != 'navNarrowMenu' && e.target.className != 'narrowLinks' && typeof e.target.className != 'object' && !open && this.state.open) {
            this.setState({open: false})
        } else if(!e) {
            this.setState({open: open && this.state.open ? false : open})
        }
    }
}

export default DccNavBar;
