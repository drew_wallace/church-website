import React, {Fragment, Component} from 'react';
import { connect } from 'react-redux';
import Modal from 'react-bootstrap/lib/Modal';
import {get, sortBy, groupBy, mapValues, keyBy} from 'lodash';
import moment from 'moment';
import DccNavBar from './dccNavBar';
import DccFooter from './dccFooter';
import Login from './login';
import {fetchPeopleIfNeeded, fetchHouseholdsIfNeeded, fetchHouseholdLeadersIfNeeded, login} from '../actions';

class Directory extends Component {
    constructor(props) {
        super(props);

        this.state = {
            options: {
                order: 'last_name', // first_name, last_name, name, member_count, //// created_at
                'where[search_name]': '', // any part of a name
                per_page: 25, // 0-100
                offset: 0 // skip this many
            },
            secondaryOrder: 'first_name', // first_name, last_name, name, member_count, //// created_at
            displayByHousehold: false,
            show: false,
            selectedPeople: {},
            selectedPersonId: '',
            selectedHouseholdId: ''
        }
    }

    render() {
        let {loggedIn, people, households, householdLeaders, fetchPeopleIfNeeded, fetchHouseholdLeadersIfNeeded, fetchHouseholdsIfNeeded, login} = this.props;

        if(loggedIn !== 'dcc') {
            return (<Login login={login} title={'Directory'} username={'dcc'}/>);
        }

        fetchPeopleIfNeeded();
        fetchHouseholdLeadersIfNeeded();
        fetchHouseholdsIfNeeded();

        let householdLeadersById = {};
        let peopleByHouseholdId = {};
        let peopleById = {};
        let peopleCount = 0;
        let selectedHousehold = [];
        if (get(people.data, 'included') && get(households.data, 'data') && get(householdLeaders.data, 'included')) {
            householdLeadersById = groupBy(householdLeaders.data.included, householdLeader => householdLeader.id);
            households.data.data = households.data.data.filter(household => householdLeadersById[household.attributes.primary_contact_id]);
            peopleCount = this.state.displayByHousehold ? households.data.data.filter(household => household.attributes.name.toLowerCase().startsWith(this.state.options['where[search_name]'].toLowerCase())).length : people.data.included.filter(person => person.attributes[this.state.options.order].toLowerCase().startsWith(this.state.options['where[search_name]'].toLowerCase())).length;
            peopleById = keyBy(people.data.included, person => person.id);
            peopleByHouseholdId = mapValues(keyBy(households.data.data, household => household.id), household => household.relationships.people.data.filter(person => peopleById[person.id]).map(person => peopleById[person.id]));
            households.data.data.forEach(household => {household.attributes.member_count = peopleByHouseholdId[household.id].length});
            selectedHousehold = peopleByHouseholdId[this.state.selectedHouseholdId] || [];
        }

        let selectedPerson = this.state.selectedPeople[this.state.selectedPersonId];

        let pagination = (
            <div className="center-content pad wrap">
                <label htmlFor="displayByHousehold" className="pagination-label">Display by Household:</label>
                <input type="checkbox" checked={this.state.displayByHousehold} onChange={event => this.displayByHousehold(event.target.checked)}/>
                <label htmlFor="per_page" className="pagination-label">Per page:</label>
                <input type="number" min={1} max={100} className="pagination-page" onChange={event => this.updateOptions('per_page', event.target.value ? Math.max(Math.min(parseInt(event.target.value), 100), 1) : '')} value={this.state.options.per_page} />
                {this.state.options.per_page &&
                    <div className="pagination-buttons-container">
                        {Array.from({length: Math.ceil(peopleCount / this.state.options.per_page)}, () => 0).map((ignoreThis, index) =>
                            <button key={index} className="pagination-button" style={index == Math.ceil(this.state.options.offset / this.state.options.per_page) ? {background: '#337ab7', color: 'white'} : {}} onClick={() => this.updateOptions('offset', index * this.state.options.per_page)}>{index + 1}</button>
                        )}
                    </div>
                }
            </div>
        );

        let list = (<div></div>);
        if (!this.state.displayByHousehold) {
            list = (
                <div className="collection-pane">
                    {people.data && sortBy(people.data.included, [o => o.attributes[this.state.options.order], o => o.attributes[this.state.secondaryOrder]]).filter(person => person.attributes[this.state.options.order].toLowerCase().startsWith(this.state.options['where[search_name]'].toLowerCase())).slice(this.state.options.offset, this.state.options.offset + this.state.options.per_page).map(person =>
                        <div className="collection-item-5">
                            <div className="person-name">
                                {`${person.attributes.first_name} ${person.attributes.last_name}`}
                            </div>
                            <div className="person-image-container">
                                <img srcSet={`${person.attributes.avatar}?g=200x200%23`} className="person-image" onClick={() => this.showPersonDialog(person.id)} />
                            </div>
                        </div>
                    )}
                </div>
            );
        } else {
            list = (
                <div className="collection-pane">
                    {get(households.data, 'data') && sortBy(households.data.data, [o => typeof o.attributes[this.state.options.order] == 'number' ? -o.attributes[this.state.options.order] : o.attributes[this.state.options.order], o => o.attributes[this.state.secondaryOrder]]).filter(household => household.attributes.name.toLowerCase().startsWith(this.state.options['where[search_name]'].toLowerCase())).slice(this.state.options.offset, this.state.options.offset + this.state.options.per_page).map(household =>
                        <div className="collection-item-5">
                            <div className="person-name">
                                {household.attributes.name} ({peopleByHouseholdId[household.id].length})
                            </div>
                            <div className="person-image-container">
                                {peopleByHouseholdId[household.id].length > 1 ?
                                    <div className="household-image" onClick={() => this.showPersonDialog(household.attributes.primary_contact_id, household.id)}>
                                        {peopleByHouseholdId[household.id].map(person =>
                                            <div>
                                                <img srcSet={`${person.attributes.avatar}?g=200x200%23`}/>
                                            </div>
                                        )}
                                    </div>
                                    :
                                    <img srcSet={`${peopleByHouseholdId[household.id][0].attributes.avatar}?g=200x200%23`} className="person-image" onClick={() => this.showPersonDialog(household.attributes.primary_contact_id)} />
                                }
                            </div>
                        </div>
                    )}
                </div>
            );
        }

        return (
            <Fragment>
                {selectedPerson &&
                    <Modal show={this.state.show} onHide={() => this.setState({show: false})}>
                        <Modal.Header closeButton>
                            <Modal.Title className="flex">
                                <div>
                                    <a target="_blank" href={selectedPerson.data.attributes.avatar}>
                                        <img srcSet={`${selectedPerson.data.attributes.avatar}?g=200x200%23`} className="person-image"/>
                                    </a>
                                </div>
                                <div className="person-modal-title-container">
                                    <div>{selectedPerson.data.attributes.name}</div>
                                    <div className="person-modal-membership">{selectedPerson.data.attributes.membership}</div>
                                    <div className="flex wrap">
                                        {selectedHousehold.length > 1 && selectedHousehold.map(person =>
                                            <span className="custom-tooltip" data-tip={person.attributes.name}>
                                                <img srcSet={`${person.attributes.avatar}?g=200x200%23`} className="person-image household" onClick={() => this.showPersonDialog(person.id, this.state.selectedHouseholdId)}/>
                                            </span>
                                        )}
                                    </div>
                                </div>
                            </Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <div className="person-modal-list-container">
                                <div>
                                    <b>Email:</b>&nbsp;
                                </div>
                                <div className="person-modal-list-item">
                                    {selectedPerson.included.filter(item => item.type == 'Email').map(email =>
                                        <div>
                                            {email.attributes.location}: <a href={`mailto:${email.attributes.address}`}>{email.attributes.address}</a>
                                        </div>
                                    )}
                                </div>
                            </div>
                            <div className="person-modal-list-container">
                                <div>
                                    <b>Address:</b>&nbsp;
                                </div>
                                <div className="person-modal-list-item">
                                    {selectedPerson.included.filter(item => item.type == 'Address').map(address =>
                                        <div>
                                            {address.attributes.location}<br/>
                                            <a target="_blank" href={`https://www.google.com/maps/place/${(address.attributes.street || '').replace(/ /, '+')},+${(address.attributes.city || '').replace(/ /, '+')},+${(address.attributes.state || '').replace(/ /, '+')}+${(address.attributes.zip || '')}`}>
                                                {address.attributes.street || ''}<br/>{address.attributes.city || ''}, {address.attributes.state || ''} {address.attributes.zip || ''}
                                            </a>
                                        </div>
                                    )}
                                </div>
                            </div>
                            <div className="person-modal-list-container"><div><b>Phone Number:</b>&nbsp;</div><div className="person-modal-list-item">{selectedPerson.included.filter(item => item.type == 'PhoneNumber').map(phoneNumber => <div>{phoneNumber.attributes.location}: <a href={`tel:${phoneNumber.attributes.number.replace(/(\d{3})(\d{3})(\d{4})/, "$1-$2-$3")}`}>{phoneNumber.attributes.number.replace(/(\d{3})(\d{3})(\d{4})/, "$1-$2-$3")}</a></div>)}</div></div>
                            <div><b>Birthday:</b> {selectedPerson.data.attributes.birthdate && moment(selectedPerson.data.attributes.birthdate).format('MMMM Do')}</div>
                        </Modal.Body>
                        <Modal.Footer>
                        <button className="link-button fill" onClick={() => this.setState({show: false})}>Close</button>
                        </Modal.Footer>
                    </Modal>
                }
                <DccNavBar/> 
                <div className="page-title directory">
                    <div className="page-title-overlay-container">
                        <div className="page-title-overlay"></div>
                    </div>
                    <div className="page-title-text-container">
                        <h1 className="text-center page-title-text">CHURCH DIRECTORY</h1>
                    </div>
                </div>
                <div className="center-content">
                    <div className="sort-filter-control">
                        <label htmlFor="sort-dropdown">Sort by:</label>
                        <select id="sort-dropdown" value={this.state.options.order} onChange={e => this.updateOptions('order', e.target.value)}>
                            {!this.state.displayByHousehold && <option value="first_name">First Name</option>}
                            {!this.state.displayByHousehold && <option value="last_name">Last Name</option>}
                            {/* !this.state.displayByHousehold && <option value="created_at">Created At</option> */}
                            {this.state.displayByHousehold && <option value="name">Name</option>}
                            {this.state.displayByHousehold && <option value="member_count">Member Count</option>}
                        </select>
                    </div>
                    <div className="sort-filter-control">
                        <input type="text" placeholder="Filter by Name" onChange={event => this.updateOptions('where[search_name]', event.target.value)} value={this.state.options['where[search_name]']}/>
                    </div>
                </div>
                {pagination}
                {list}
                {pagination}
                <DccFooter/> 
            </Fragment>
        );
    }

    updateOptions(key, value) {
        let newState = {...this.state.options, [key]: value};
        let secondaryOrder = this.state.secondaryOrder;
        if(key == 'per_page' || key == 'where[search_name]') {
            newState.offset = 0;
        }
        if(key == 'order') {
            secondaryOrder = this.state.secondaryOrder == 'first_name' ? 'last_name' : 'first_name';
            if (this.state.displayByHousehold) {
                secondaryOrder = this.state.secondaryOrder == 'name' ? 'member_count' : 'name';
            }
        }
        this.setState({options: newState, secondaryOrder});
    }

    displayByHousehold(checked) {
        let newState = {...this.state.options, order: 'last_name', offset: 0};
        let secondaryOrder = 'first_name';
        if (checked) {
            newState = {...this.state.options, order: 'name', offset: 0};
            secondaryOrder = 'member_count';
        }
        this.setState({options: newState, displayByHousehold: checked, secondaryOrder});
    }

    showPersonDialog(personId, householdId) {
        if(!this.state.selectedPeople[personId]) {
            fetch(`/api/people/${personId}`, {credentials: 'include'})
            .then(response => {
                return response.json();
            }).then(data => {
                this.state.selectedPeople[data.data.id] = data;
                this.setState({selectedPeople: this.state.selectedPeople, selectedPersonId: data.data.id, show: true, selectedHouseholdId: householdId})
            }).catch(err => {
                console.log('parsing failed', err)
            });
        } else {
            this.setState({show: true, selectedPersonId: personId, selectedHouseholdId: householdId})
        }
    }
};

const mapStateToProps = (state) => {
    return {
        loggedIn: state.loggedIn,
        people: state.people,
        householdLeaders: state.householdLeaders,
        households: state.households
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchPeopleIfNeeded: () => dispatch(fetchPeopleIfNeeded()),
        fetchHouseholdsIfNeeded: () => dispatch(fetchHouseholdsIfNeeded()),
        fetchHouseholdLeadersIfNeeded: () => dispatch(fetchHouseholdLeadersIfNeeded()),
        login: (user) => dispatch(login(user))
    };
}

const DirectoryLayout = connect(
    mapStateToProps,
    mapDispatchToProps
)(Directory);

export default DirectoryLayout;
