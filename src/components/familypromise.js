import React, {Fragment, Component} from 'react';
import { connect } from 'react-redux';
import DccNavBar from './dccNavBar';
import DccFooter from './dccFooter';

const FamilyPromise = () => (
    <Fragment>
        <DccNavBar/>
        <div className="page-title fp">
            <div className="page-title-overlay-container">
                <div className="page-title-overlay"></div>
            </div>
            <div className="page-title-text-container">
                <h1 className="text-center page-title-text">Family Promise</h1>
            </div>
        </div>
        <div>
            <div className="familypromise">
                <h3>If you wish to volunteer with Family Promise of North Fulton/DeKalb, please do the following:</h3>
                <ul>
					<li>Watch <a target="_blank" href="https://www.youtube.com/watch?v=rorEIi50qqQ&authuser=1">the following 15 min. video</a> on Family Promise.</li>
					<li>Read <a target="_blank" href="https://dccsiteimages.s3.amazonaws.com/familypromise/Family Promise - Overview.pdf">this document</a> about Family Promise.</li>
					<li>Read <a target="_blank" href="https://dccsiteimages.s3.amazonaws.com/familypromise/Family Promise - Best Practices for Volunteers.pdf">this document</a> about best practices for volunteers.</li>
					<li>Print out and sign <a target="_blank" href="https://dccsiteimages.s3.amazonaws.com/familypromise/Family Promise - Confidentiality Agreement.pdf">this form</a> stating that you understand the need for confidentiality as a volunteer.  Hard copies of the form are also available in the church office to sign.</li>
				</ul>
                <p>Once you've finished, please drop your form off at the church office.</p>

                <hr/>
                <p>DCC is both a "host" church where we occasionally host families in the church's facilities and also a "support" church for Kingswood United Methodist Church (KUMC) where they host the families in their church facility and we help with volunteer duties. KUMC is at the corner of Tilly Mill and North Peachtree.</p>

                <p>Our next volunteer sessions are the week of June 9 - 16 when we will help support KUMC as they host the families and the following week of June 16 - 23 when we will host the families at DCC.</p>

                <p><a target="_blank" href="https://www.signupgenius.com/go/5080B4CACA82DABF49-family">Click here</a> to volunteer to help support Kingswood United Methodist as they host the families the week of June 9 - 16.
				<br/>
                <a target="_blank" href="https://www.signupgenius.com/go/5080F4EAFAB2DA31-volunteers1">Click here</a> to volunteer to help host the families at DCC the week of June 16 - 23.</p>

                <p>Thanks so much for being willing to help out.</p>
            </div>
        </div>
        <DccFooter/>
    </Fragment>
);

export default FamilyPromise;
