import React, {Component, Fragment} from 'react';
import { connect } from 'react-redux';
import {get, keyBy, uniqBy, map, values} from 'lodash';
import moment from 'moment';
import Select, {Creatable} from 'react-select';
import 'react-select/dist/react-select.css';
import 'event-source-polyfill';
import checkStatus from '../lib/checkStatus';
import DccFooter from './dccFooter';
import DccNavBar from './dccNavBar';
import Login from './login';
import {ToastContainer, toast} from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css';
import Modal from 'react-bootstrap/lib/Modal';
import {login, fetchMediaIfNeeded, invalidateMedia} from '../actions';

class PodcastAdmin extends Component {
    constructor(props) {
        super(props);

        this.state = {
            imageFileName: '',
            previousImage: '',
            audioFileName: '',
            title: '',
            speaker: '',
            series: '',
            episode: '',
            pubDate: '',
            videoLink: '',
            show: false,
            fileUploads: null,
            submitted: false
        };
    }

    componentWillReceiveProps(newProps) {
        if (newProps.loggedIn === 'dccadmin' && !this.es) {
            this.es = new EventSource('/api/sse');
            this.es.addEventListener('s3UploadStatus', this.readSse);
        }

        let episodesByGuid = {};

        if (get(newProps.media.data, 'rss.channel.item')) {
            episodesByGuid = keyBy(newProps.media.data.rss.channel.item, 'guid');
        }

        const selectedEpisode = episodesByGuid[get(this.state.episode, 'value')];

        if (selectedEpisode) {
            this.setState({
                episode: {label: selectedEpisode.title || '<no title>', value: selectedEpisode.guid},
                title: selectedEpisode.title || '',
                speaker: selectedEpisode['itunes:author'] || '',
                series: selectedEpisode['itunes:summary'] || '',
                pubDate: selectedEpisode.pubDate,
                videoLink : selectedEpisode.videoLink || '',
                imageFileName: '',
                audioFileName: ''
            });
        } else {
            this.setState({
                fileUploads: null,
                previousImage: '',
                submitted: false
            });
        }
    }

    readSse = (e) => {
        let fileUpload = JSON.parse(e.data);

        if (!this.state.fileUploads || !this.state.fileUploads[fileUpload.action + fileUpload.file]) {
            const toastId = toast.info(`${fileUpload.action} ${fileUpload.file}: ${fileUpload.percent}`, {
                position: 'bottom-right',
                autoClose: false,
                closeOnClick: false,
                draggable: false,
            });

            this.setState({fileUploads: {...this.state.fileUploads, [fileUpload.action + fileUpload.file]: toastId}});
        } else {
            const toastId = this.state.fileUploads[fileUpload.action + fileUpload.file];
            if (fileUpload.done) {
                toast.dismiss(toastId);
            } else {
                toast.update(toastId, {
                    render: `${fileUpload.action} ${fileUpload.file}: ${fileUpload.percent}`
                });
            }
        }
    }

    closeSse = () => {
        if (this.es) {
            this.es.removeEventListener('s3UploadStatus', this.readSse);
            this.es.close();
            this.es = undefined;
        }
    }

    componentDidMount() {
        if (this.props.loggedIn === 'dccadmin' && !this.es) {
            this.es = new EventSource('/api/sse');
            this.es.addEventListener('s3UploadStatus', this.readSse);
        }
    }

    componentWillUnmount() {
        this.closeSse();
    }

    render() {
        let {loggedIn, login, media, fetchMediaIfNeeded} = this.props;

        if(loggedIn !== 'dccadmin') {
            return (<Login login={login} title={'Podcast Admin'} username={'dccadmin'}/>);
        }

        fetchMediaIfNeeded();

        let series = [];
        let speakers = [];
        let previousImages = [];
        let episodes = [];
        let episodesByGuid = {};

        if (get(media.data, 'rss.channel.item')) {
            series = uniqBy(media.data.rss.channel.item.filter(media => media['itunes:summary']).map(media => {return {label: media['itunes:summary'], value: media['itunes:summary']}}), 'value');
            speakers = uniqBy(media.data.rss.channel.item.filter(media => media['itunes:author']).map(media => {return {label: media['itunes:author'], value: media['itunes:author']}}), 'value');
            previousImages = uniqBy(media.data.rss.channel.item.filter(media => media['itunes:image'].href).map(media => {return {label: media['itunes:image'].href, value: media['itunes:image'].href.replace(/^.*[\\\/]/, '')}}), 'value');
            episodes = media.data.rss.channel.item.map(media => {return {label: media.title || '<no title>', value: media.guid}});
            episodes.unshift({label: 'New Episode', value: ''});
            episodesByGuid = keyBy(media.data.rss.channel.item, 'guid');
        }

        const selectedEpisode = episodesByGuid[get(this.state.episode, 'value')];

        return (
            <Fragment>
                <ToastContainer />
                <DccNavBar/>
                <div className="podcast-admin-container">
                    <div className="center-content margin">
                        <Select
                            value={this.state.episode}
                            onChange={selectable => this.changeEpisode(selectable, episodesByGuid)}
                            placeholder="Episode"
                            options={episodes}
                            className="form-field-widths"
                        />
                    </div>
                    <form key="form" ref="form" className="center-content column">
                        <div><input type="text" name="title" placeholder="Title" className="form-field-widths podcast-title-input" value={this.state.title} onChange={e => this.setState({title: e.target.value})}/></div>
                        <div>
                            <Creatable
                                value={this.state.speaker}
                                onChange={selectable => this.setState({speaker: selectable})}
                                name="author"
                                placeholder="Speaker"
                                options={speakers}
                                className="form-field-widths"
                            />
                        </div>
                        <div>
                            <Creatable
                                value={this.state.series}
                                onChange={selectable => this.setState({series: selectable})}
                                name="summary"
                                placeholder="Series"
                                options={series}
                                className="form-field-widths"
                            />
                        </div>
                        <div><input type="text" name="videoLink" placeholder="Video Link" className="form-field-widths podcast-title-input" value={this.state.videoLink} onChange={e => this.setState({videoLink: e.target.value})}/></div>
                        <div>
                            <input placeholder="Publish date:" type="date" name="pubDate" className="form-field-widths podcast-date-input" value={this.state.pubDate ? moment(this.state.pubDate).format('YYYY-MM-DD') : ''} onChange={e => this.setState({pubDate: moment(e.target.value, "YYYY-MM-DD").locale('en').format('ddd, DD MMM YYYY [10:30:00] ZZ')})}/>
                        </div>
                        <div className="center-content column bordered pad margin">
                            <h4 className="text-center">Files</h4>
                            {selectedEpisode && selectedEpisode['itunes:image'].href && !this.state.imageFileName &&
                                <img srcSet={`${selectedEpisode['itunes:image'].href}`} width="120px"/>
                            }
                            <label className="fileLabel">
                                <input key="image" name="image" type="file" accept="image/png, image/jpeg" onChange={e => this.setState({imageFileName: e.target.value.replace(/^.*[\\\/]/, '')})}/>
                                <span key="image-label">{this.state.imageFileName || 'Image'}</span>
                            </label>
                            or
                            <Creatable
                                value={this.state.previousImage}
                                onChange={selectable => this.setState({previousImage: selectable})}
                                name="previousImage"
                                placeholder="Previous Image"
                                options={previousImages}
                                className="form-field-widths image-select"
                                optionRenderer={(option) => (
                                    <div>{option.value}<br/><img width="200px" srcSet={option.label}/></div>
                                )}
                                valueRenderer={(option) => (
                                    <div>{option.value}<br/><img width="200px" srcSet={option.label} /></div>
                                )}
                            />
                            {selectedEpisode && !this.state.audioFileName &&
                                <audio controls className="margin" ref="audio" key={`${selectedEpisode.guid}Audio`}>
                                    <source src={`${selectedEpisode.enclosure.url}?cacheBuster=${(new Date()).valueOf()}`} type={selectedEpisode.enclosure.type} />
                                </audio>
                            }
                            <label className="fileLabel">
                                <input key="audio" name="audio" type="file" accept="audio/mp3" onChange={e => this.setState({audioFileName: e.target.value.replace(/^.*[\\\/]/, '')})}/>
                                <span key="audio-label">{this.state.audioFileName || 'Audio'}</span>
                            </label>
                        </div>
                    </form>
                    <div className="center-content margin-bottom">
                        {selectedEpisode &&
                            <Fragment>
                                <button className="link-button caution margin" onClick={() => this.setState({show: true})}>Remove</button>
                                <div style={{borderLeft: '1px solid black', height: 60, margin: '0px 55px'}}>&nbsp;</div>
                            </Fragment>
                        }
                        <button className="link-button" disabled={this.state.submitted} onClick={() => this.uploadPodcast(selectedEpisode)}>Submit</button>
                    </div>
                </div>
                <DccFooter/>
                {selectedEpisode &&
                    <Modal show={this.state.show} onHide={() => this.setState({show: false})}>
                        <Modal.Header closeButton>
                            <Modal.Title>
                                Remove Podcast
                            </Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            Are you sure you want to delete?<br/>
                            <br/>
                            Title: {selectedEpisode.title}<br/>
                            GUID: {selectedEpisode.guid}
                        </Modal.Body>
                        <Modal.Footer>
                            <button className="link-button caution" onClick={() => this.removePodcast(selectedEpisode)}>Remove</button>
                            <button className="link-button fill" onClick={() => this.setState({show: false})}>Close</button>
                        </Modal.Footer>
                    </Modal>
                }
            </Fragment>
        );
    }

    changeEpisode(selectable, episodesByGuid) {
        if (!selectable) {
            selectable = {label: 'New Episode', value: ''};
        }

        const selectedEpisode = episodesByGuid[selectable.value];

        this.setState({
            episode: selectable,
            title: selectable.value && selectedEpisode.title || '',
            speaker: selectable.value && selectedEpisode['itunes:author'] || '',
            series: selectable.value && selectedEpisode['itunes:summary'] || '',
            videoLink: selectable.value && selectedEpisode.videoLink || '',
            pubDate: selectable.value && selectedEpisode.pubDate || '',
            previousImage: ''
        });
    }

    uploadPodcast(selectedEpisode) {
        this.setState({submitted: true});
        const self = this;
        const toastId = toast.info(`Upload Progress: 0%`, {
            position: 'bottom-right',
            autoClose: false,
            closeOnClick: false,
            draggable: false,
        });
        const xhr = new XMLHttpRequest();
		xhr.timeout = 180000;
        xhr.responseType = 'json';

        if (selectedEpisode) {
            xhr.open('PUT', `/api/podcast/${selectedEpisode.guid.replace('http://dunwoodychurch.org/media?id=', '')}`, true);
        } else {
            xhr.open('POST', '/api/podcast', true);
        }

        xhr.upload.onprogress = function(e) {
            if (e.lengthComputable) {
                const percentage = ((e.loaded / e.total) * 100).toFixed(0);
                toast.update(toastId, {
                    render: `Upload Progress: ${percentage}%`
                });
                if (percentage === '100') {
                    toast.dismiss(toastId);
                }
            }
        };

        xhr.onerror = (e) => {
            toast.dismiss(toastId);
            toast.error('Looks like there is a Network error', {
                position: 'top-center',
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                draggablePercent: 60
            });
            this.setState({submitted: false});
            console.error(e);
        };

        xhr.onload = function() {
            if (this.status >= 200 && this.status < 300) {
                self.props.invalidateMedia();
                toast.success(this.response.message, {
                    position: 'top-center',
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    draggablePercent: 60
                });
            } else {
                toast.error(this.response.message || 'An error occured on the server', {
                    position: 'top-center',
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    draggablePercent: 60
                });
                console.error(this.response);
            }
            toast.dismiss(toastId);
            self.setState({submitted: false});
        };

        xhr.send(new FormData(this.refs.form));
    }

    removePodcast(selectedEpisode) {
        fetch(`/api/podcast/${selectedEpisode.guid.replace('http://dunwoodychurch.org/media?id=', '')}`, {
            method: 'DELETE',
            credentials: 'include'
        }).then(checkStatus)
        .then((response) => response.json())
        .then((data) => {
            this.props.invalidateMedia();
            this.setState({
                fileUploads: null,
                imageFileName: '',
                audioFileName: '',
                title: '',
                videoLink: '',
                speaker: '',
                series: '',
                episode: '',
                pubDate: '',
                previousImage: '',
                show: false
            });
            toast.success(data.message, {
                position: 'top-center',
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                draggablePercent: 60
            });
        }).catch(async (err) => {
            const response = await err.response.json();
            toast.error(response.message, {
                position: 'top-center',
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                draggablePercent: 60
            });
            console.error(response);
        });
    }
};

const mapStateToProps = (state) => {
    return {
        loggedIn: state.loggedIn,
        media: state.media
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        login: (user) => dispatch(login(user)),
        fetchMediaIfNeeded: () => dispatch(fetchMediaIfNeeded()),
        invalidateMedia: () => dispatch(invalidateMedia())
    };
}

const PodcastAdminLayout = connect(
    mapStateToProps,
    mapDispatchToProps
)(PodcastAdmin);

export default PodcastAdminLayout;
