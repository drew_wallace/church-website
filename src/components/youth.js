import React, {Fragment, Component} from 'react';
import { connect } from 'react-redux';
import DccNavBar from './dccNavBar';
import DccFooter from './dccFooter';
import {Link} from 'react-router-dom';
import {groupBy, keyBy, map, get, mapValues, keys, last} from 'lodash';
import moment from 'moment';
import md from '../lib/markdown';
import {fetchYouthEventsIfNeeded} from '../actions';

class Youth extends Component {
    constructor(props) {
        super(props);

        this.state = {
            display: 'date'
        }
    }

    render() {
        let {youthEvents, fetchYouthEventsIfNeeded} = this.props;

        fetchYouthEventsIfNeeded();

        let youthEventsEventType = {};
        let youthEventsByDate = {};
        let youthEventsByWeekday = {};
        let youthDatesByEventType = {};
        if (get(youthEvents.data, 'data')) {
            youthEventsEventType = keyBy(youthEvents.data.included, event => event.id);
            youthEventsByDate = groupBy(youthEvents.data.data, booking => moment(booking.attributes.starts_at).format('dddd, MMM D'));
            youthEventsByWeekday = groupBy(youthEvents.data.data, booking => moment(booking.attributes.starts_at).format('dddd'));
            youthDatesByEventType = mapValues(groupBy(youthEvents.data.data, booking => booking.relationships.event.data.id), bookings => bookings.map(booking => `${moment(booking.attributes.starts_at).format('dddd, MMM D h:mma')} - ${moment(booking.attributes.starts_at).format('MM/DD/YYYY') == moment(booking.attributes.ends_at).format('MM/DD/YYYY') ? moment(booking.attributes.ends_at).format('h:mma') : moment(booking.attributes.ends_at).format('dddd, MMM D h:mma')}`));
        }

        let eventsList;
        switch (this.state.display) {
            case 'event':
                eventsList = map(youthDatesByEventType, (dates, eventId) =>
                    <Fragment>
                        <div className="split-pane-container center-content">
                            <div className="event-image-container">
                                <img className="event-image" srcSet={get(youthEventsEventType[eventId], 'relationships.attachments.data[0].attributes.url', 'https://dccsiteimages.s3.amazonaws.com/dcc_leaf_logo.png')} />
                            </div>
                            <div className="event-details-container">
                                <div className="event-title">
                                    {youthEventsEventType[eventId].attributes.name}
                                </div>
                                <div className="event-details" dangerouslySetInnerHTML={{__html: md.render(youthEventsEventType[eventId].attributes.details)}} />
                                {dates.map(date =>
                                    <div className="event-date">
                                        {date}
                                    </div>
                                )}
                            </div>
                        </div>
                        {eventId != last(keys(youthDatesByEventType)) &&
                            <hr />
                        }
                    </Fragment>
                );
                break;
            case 'weekday':
                eventsList = map(youthEventsByWeekday, (events, day) =>
                    <Fragment>
                        <h3>Upcoming {day}s</h3>
                        {events.map((event, index) =>
                            <Fragment>
                                <div className="split-pane-container center-content">
                                    <div className="event-image-container">
                                        <img className="event-image" srcSet={get(youthEventsEventType[event.relationships.event.data.id], 'relationships.attachments.data[0].attributes.url', 'https://dccsiteimages.s3.amazonaws.com/dcc_leaf_logo.png')} />
                                    </div>
                                    <div className="event-details-container">
                                        <div className="event-date">
                                            {`${moment(event.attributes.starts_at).format('MMM D @ h:mma')} - ${moment(event.attributes.starts_at).format('MM/DD/YYYY') == moment(event.attributes.ends_at).format('MM/DD/YYYY') ? moment(event.attributes.ends_at).format('h:mma') : moment(event.attributes.ends_at).format('MMM D @ h:mma')}`}
                                        </div>
                                        <div className="event-title">
                                            {youthEventsEventType[event.relationships.event.data.id].attributes.name}
                                        </div>
                                        <div className="event-details" dangerouslySetInnerHTML={{__html: md.render(youthEventsEventType[event.relationships.event.data.id].attributes.details)}} />
                                    </div>
                                </div>
                                {index != events.length - 1 && <hr className="thin dashed" />}
                            </Fragment>
                        )}
                        {day != last(keys(youthEventsByWeekday)) &&
                            <hr />
                        }
                    </Fragment>
                );
                break;
            case 'date':
            default:
                eventsList = map(youthEventsByDate, (events, date) =>
                    <Fragment>
                        <h3>{date}</h3>
                        {events.map((event, index) =>
                            <Fragment>
                                <div className="split-pane-container center-content">
                                    <div className="event-image-container">
                                        <img className="event-image" srcSet={get(youthEventsEventType[event.relationships.event.data.id], 'relationships.attachments.data[0].attributes.url', 'https://dccsiteimages.s3.amazonaws.com/dcc_leaf_logo.png')} />
                                    </div>
                                    <div className="event-details-container">
                                        <div className="event-date">
                                            {`${moment(event.attributes.starts_at).format('h:mma')} - ${moment(event.attributes.starts_at).format('MM/DD/YYYY') == moment(event.attributes.ends_at).format('MM/DD/YYYY') ? moment(event.attributes.ends_at).format('h:mma') : moment(event.attributes.ends_at).format('MMM D h:mma')}`}
                                        </div>
                                        <div className="event-title">
                                            {youthEventsEventType[event.relationships.event.data.id].attributes.name}
                                        </div>
                                        <div className="event-details" dangerouslySetInnerHTML={{__html: md.render(youthEventsEventType[event.relationships.event.data.id].attributes.details)}} />
                                    </div>
                                </div>
                                {index != events.length - 1 && <hr className="thin dashed" />}
                            </Fragment>
                        )}
                        {date != last(keys(youthEventsByDate)) &&
                            <hr />
                        }
                    </Fragment>
                );
                break;
        }

        return (
            <Fragment>
                <DccNavBar/>
                <div className="page-title youth">
                    <div className="page-title-overlay-container">
                        <div className="page-title-overlay"></div>
                    </div>
                    <div className="page-title-text-container">
                        <h1 className="text-center page-title-text">CHILDREN & YOUTH</h1>
                    </div>
                </div>
                <div className="split-pane-container">
                    <div className="split-pane-left">
                        <h2 className="text-center">Children's Ministry</h2>
                        <p>
                            The Dunwoody Community Church Children’s Ministry exists to partner with parents in raising children to be disciples who make disciples.
                        </p>
                        <p>
							Here in DCC Children's Ministry, our goal is to help children encounter God in an authentic way, engage with Him on an age-appropriate level, and embrace Him more deeply with each passing year. To accomplish this goal, we strive to teach the Gospel, instill Christian disciplines, and build relationships within our classrooms each Sunday morning.
                        </p>
                        <p>
							We pray that as a result, students will graduate to youth group with a firm foundation of knowledge about who God is and what He has done for them. We pray that students will become disciples who make disciples in our surrounding communities and neighborhoods, to the glory of God.
						</p>
						<p>
							If you'd like to read more about our Core Competencies, you can click <a target="_blank" href="https://dccresources.s3.amazonaws.com/Core+Competencies+%26+Expectations.pdf">HERE</a>.
						</p>
						<hr />
						<h2 className="text-center">First Time Guests</h2>
						<p>
							DCC Children's Ministry begins check-in at 10:20 AM on Sunday morning. When you arrive, our Children's Greeters will assist you in finding the correct class for your child. We have four classes for children: Nursery (birth - walking), Toddlers (walking - age 2), Preschool/Pre-K (age 3 - age 4), K-1st Grade, and 2nd-5th grade.
						</p>
						<p>
							You will check your child in at the classroom door using our electronic system, and you and your child will be given matching security tags. You will be asked to enter a phone number and any allergies your child has. Your child will wear their security tag until you return to check out.
						</p>
						<p>
							During checkout, you will return to your child's room and show your tag in order for your child to be released to you. If your child needs you during the service, we will text the number provided during the check-in process.
						</p>
						<p>
							We look forward to meeting you this Sunday!
						</p>
                        <div>
                            {/*<div className="collection-pane">
                                <div className="collection-item-2 medium">
                                    <div className="childrens-block-container">
                                        <div className="childrens-block-overlay-container">
                                            <div className="childrens-block-overlay"></div>
                                        </div>
                                        <div className="childrens-block-content">
                                            <div>
                                                <h3 className="text-center">Nursery & Toddlers</h3>
                                                <p className="text-center side-pad">Building a foundation of trust and support between parents, infants, and Sunday School teachers. To lay the groundwork of <b>being with Jesus</b>.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="collection-item-2 medium">
                                    <div className="childrens-block-container">
                                        <div className="childrens-block-overlay-container">
                                            <div className="childrens-block-overlay"></div>
                                        </div>
                                        <div className="childrens-block-content">
                                            <div>
                                                <h3 className="text-center">Preschool/Pre-K</h3>
                                                <p className="text-center side-pad">To learn who Jesus is so we can <b>be with Jesus</b> and <b>be like Jesus</b>.<br/><i>God is ______.</i><br/>(Characteristics of God)</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="collection-item-2 medium">
                                    <div className="childrens-block-container">
                                        <div className="childrens-block-overlay-container">
                                            <div className="childrens-block-overlay"></div>
                                        </div>
                                        <div className="childrens-block-content">
                                            <div>
                                                <h3 className="text-center">K-1st Grade</h3>
                                                <p className="text-center side-pad"> To encourage a personal response to Jesus so we can <b>do what Jesus did</b>.<br/><i>God is _____, therefore I ______.</i><br/>(Personal response to God)</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="collection-item-2 medium">
                                    <div className="childrens-block-container">
                                        <div className="childrens-block-overlay-container">
                                            <div className="childrens-block-overlay"></div>
                                        </div>
                                        <div className="childrens-block-content">
                                            <div>
                                                <h3 className="text-center">Children's Church (1<span className="ordinal">st</span>-5<span className="ordinal">th</span> Grade)</h3>
                                                <p className="text-center side-pad">To develop personal beliefs so we can begin to think about discipling other to <b>be with Jesus</b>, <b>be like Jesus</b>, and <b>do what Jesus did</b>.<br/><i>God is _____, therefore I _____, because I believe ______.</i><br/>(Building personal beliefs)</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div> */}
                            <hr />
                            <div className="center-content column">
                                {/*<a target="_blank" href="https://dccsiteimages.s3.amazonaws.com/files/kids_ministry_philosophy.pdf" className="link-button">Philosophy of Children's Ministry</a>
                                <a target="_blank" href="https://dccsiteimages.s3.amazonaws.com/files/kids_ministry_age_goals.pdf" className="link-button">Age Level Goals</a> */}
                                <Link to="/teacherportal" className="link-button">Teacher & Parent Portal</Link>
                            </div>
                        </div>
                    </div>
                    <div className="split-pane-right">
                        <div className="center-content column">
                            <h2 className="text-center">Youth Group</h2>
                            <p>At DCC youth group, our goal is for every student to hear the truth of Jesus Christ & encounter God in an authentic, personal way. Youth group offers opportunities to <b>be with Jesus</b>, <b>be like Jesus</b>, and <b>do what Jesus did</b>. Join us at <b>D-Nite</b>, a weekly gathering of students on Sunday nights at 6:30. Together we study Scripture, pray, and build our community as the body of Christ. We hope to see you there!</p>
                            <img srcSet="https://dccsiteimages.s3.amazonaws.com/youth-group.jpg" width="100%"/>
                            <div>
                                <h2 className="text-center">Youth Group Events</h2>
                                <div className="center-content column">
                                    <div className="pad">
                                        <label>Display by:</label>
                                        <select value={this.state.display} onChange={e => this.setState({display: e.target.value})}>
                                            <option value="date">Date</option>
                                            <option value="event">Event</option>
                                            <option value="weekday">Weekday</option>
                                        </select>
                                    </div>
                                </div>
                                <hr />
                                {eventsList}
                            </div>
                        </div>
                    </div>
                </div>
                <DccFooter/>
            </Fragment>
        );
    }
};

const mapStateToProps = (state) => {
    return {
        youthEvents: state.youthEvents
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchYouthEventsIfNeeded: () => dispatch(fetchYouthEventsIfNeeded())
    };
}

const YouthLayout = connect(
    mapStateToProps,
    mapDispatchToProps
)(Youth);

export default YouthLayout;
