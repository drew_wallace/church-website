import React, {Fragment, Component} from 'react';
import { connect } from 'react-redux';
import DccNavBar from './dccNavBar';
import DccFooter from './dccFooter';
import {groupBy, keyBy, map, get, mapValues, keys, last} from 'lodash';
import moment from 'moment';
import md from '../lib/markdown';
import {fetchChurchEventsIfNeeded, fetchSpecialEventsIfNeeded} from '../actions';

class Events extends Component {
    constructor(props) {
        super(props);

        this.state = {
            display: 'date'
        }
    }

    render() {
        let {churchEvents, fetchChurchEventsIfNeeded, specialEvents, fetchSpecialEventsIfNeeded} = this.props;

        fetchChurchEventsIfNeeded();
        fetchSpecialEventsIfNeeded();

        let churchEventsEventType = {};
        let churchEventsByDate = {};
        let churchEventsByWeekday = {};
        let churchDatesByEventType = {};
        if (get(churchEvents.data, 'data')) {
            churchEventsEventType = keyBy(churchEvents.data.included, event => event.id);
            churchEventsByDate = groupBy(churchEvents.data.data, booking => moment(booking.attributes.starts_at).format('dddd, MMM D'));
            churchEventsByWeekday = groupBy(churchEvents.data.data, booking => moment(booking.attributes.starts_at).format('dddd'));
            churchDatesByEventType = mapValues(groupBy(churchEvents.data.data, booking => booking.relationships.event.data.id), bookings => bookings.map(booking => `${moment(booking.attributes.starts_at).format('dddd, MMM D h:mma')} - ${moment(booking.attributes.starts_at).format('MM/DD/YYYY') == moment(booking.attributes.ends_at).format('MM/DD/YYYY') ? moment(booking.attributes.ends_at).format('h:mma') : moment(booking.attributes.ends_at).format('dddd, MMM D h:mma')}`));
        }

        let specialEventsEventType = {};
        if (get(specialEvents.data, 'data')) {
            specialEventsEventType = keyBy(specialEvents.data.included, event => event.id);
        }

        let regularEvents;
        switch (this.state.display) {
            case 'event':
                regularEvents = map(churchDatesByEventType, (dates, eventId) =>
                    <Fragment>
                        <div className="split-pane-container center-content">
                            <div className="event-image-container">
                                <img className="event-image" srcSet={get(churchEventsEventType[eventId], 'relationships.attachments.data[0].attributes.url', 'https://dccsiteimages.s3.amazonaws.com/dcc_logo.png')} />
                            </div>
                            <div className="event-details-container">
                                <div className="event-title">
                                    {churchEventsEventType[eventId].attributes.name}
                                </div>
                                <div className="event-details" dangerouslySetInnerHTML={{__html: md.render(churchEventsEventType[eventId].attributes.details)}} />
                                {dates.map(date =>
                                    <div className="event-date">
                                        {date}
                                    </div>
                                )}
                            </div>
                        </div>
                        {eventId != last(keys(churchDatesByEventType)) &&
                            <hr />
                        }
                    </Fragment>
                );
                break;
            case 'weekday':
                regularEvents = map(churchEventsByWeekday, (events, date) =>
                    <Fragment>
                        <h3>Upcoming {date}s</h3>
                        {events.map((event, index) =>
                            <Fragment>
                                <div className="split-pane-container center-content">
                                    <div className="event-image-container">
                                        <img className="event-image" srcSet={get(churchEventsEventType[event.relationships.event.data.id], 'relationships.attachments.data[0].attributes.url', 'https://dccsiteimages.a/dcc_logo.png')} />
                                    </div>
                                    <div className="event-details-container">
                                        <div className="event-date">
                                            {`${moment(event.attributes.starts_at).format('MMM D @ h:mma')} - ${moment(event.attributes.starts_at).format('MM/DD/YYYY') == moment(event.attributes.ends_at).format('MM/DD/YYYY') ? moment(event.attributes.ends_at).format('h:mma') : moment(event.attributes.ends_at).format('MMM D @ h:mma')}`}
                                        </div>
                                        <div className="event-title">
                                            {churchEventsEventType[event.relationships.event.data.id].attributes.name}
                                        </div>
                                        <div className="event-details" dangerouslySetInnerHTML={{__html: md.render(churchEventsEventType[event.relationships.event.data.id].attributes.details)}} />
                                    </div>
                                </div>
                                {index != events.length - 1 && <hr className="thin dashed" />}
                            </Fragment>
                        )}
                        {date != last(keys(churchEventsByWeekday)) &&
                            <hr />
                        }
                    </Fragment>
                );
                break;
            case 'date':
            default:
                regularEvents = map(churchEventsByDate, (events, date) =>
                    <Fragment>
                        <h3>{date}</h3>
                        {events.map((event, index) =>
                            <Fragment>
                                <div className="split-pane-container center-content">
                                    <div className="event-image-container">
                                        <img className="event-image" srcSet={get(churchEventsEventType[event.relationships.event.data.id], 'relationships.attachments.data[0].attributes.url', 'https://dccsiteimages.s3.amazonaws.com/dcc_logo.png')} />
                                    </div>
                                    <div className="event-details-container">
                                        <div className="event-date">
                                            {`${moment(event.attributes.starts_at).format('h:mma')} - ${moment(event.attributes.starts_at).format('MM/DD/YYYY') == moment(event.attributes.ends_at).format('MM/DD/YYYY') ? moment(event.attributes.ends_at).format('h:mma') : moment(event.attributes.ends_at).format('MMM D h:mma')}`}
                                        </div>
                                        <div className="event-title">
                                            {churchEventsEventType[event.relationships.event.data.id].attributes.name}
                                        </div>
                                        <div className="event-details" dangerouslySetInnerHTML={{__html: md.render(churchEventsEventType[event.relationships.event.data.id].attributes.details)}} />
                                    </div>
                                </div>
                                {index != events.length - 1 && <hr className="thin dashed"/>}
                            </Fragment>
                        )}
                        {date != last(keys(churchEventsByDate)) &&
                            <hr />
                        }
                    </Fragment>
                );
                break;
        }

        return (
            <Fragment>
                <DccNavBar/>
                <div className="page-title events">
                    <div className="page-title-overlay-container">
                        <div className="page-title-overlay"></div>
                    </div>
                    <div className="page-title-text-container">
                        <h1 className="text-center page-title-text">What's Happening at DCC</h1>
                    </div>
                </div>
                <div className="split-pane-container reverse">
                    <div className="split-pane-left">
                        <h2 className="text-center">Regular Events</h2>
                        <div className="center-content column">
                            <div className="pad">
                                <label>Display by:</label>
                                <select value={this.state.display} onChange={e => this.setState({display: e.target.value})}>
                                    <option value="date">Date</option>
                                    <option value="event">Event</option>
                                    <option value="weekday">Weekday</option>
                                </select>
                            </div>
                        </div>
                        <hr/>
                        {this.state.display == 'event'}
                        {regularEvents}
                    </div>
                    <div className="split-pane-right">
                        <h2 className="text-center">Special Events</h2>
                        <div className="center-content column special-events-padding">
                            <div className="pad">
                                <label>Display by:</label>
                                <select defaultValue="date">
                                    <option value="date">Date</option>
                                    <option value="event">Event</option>
                                    <option value="weekday">Weekday</option>
                                </select>
                            </div>
                        </div>
                        <hr/>
                        {get(specialEvents.data, 'data') && specialEvents.data.data.map(event =>
                            <Fragment>
                                <div className="split-pane-container center-content">
                                    <div className="event-image-container">
                                        <img className="event-image" srcSet={get(specialEventsEventType[event.relationships.event.data.id], 'relationships.attachments.data[0].attributes.url', 'https://dccsiteimages.s3.amazonaws.com/dcc_logo.png')}/>
                                    </div>
                                    <div className="event-details-container">
                                        <div className="event-date">
                                            {`${moment(event.attributes.starts_at).format('dddd, MMM D @ h:mma')} - ${moment(event.attributes.starts_at).format('MM/DD/YYYY') == moment(event.attributes.ends_at).format('MM/DD/YYYY') ? moment(event.attributes.ends_at).format('h:mma') : moment(event.attributes.ends_at).format('dddd, MMM D @ h:mma')}`}
                                        </div>
                                        <div className="event-title">
                                            {specialEventsEventType[event.relationships.event.data.id].attributes.name}
                                        </div>
                                        <div className="event-details" dangerouslySetInnerHTML={{__html: md.render(specialEventsEventType[event.relationships.event.data.id].attributes.details)}}/>
                                    </div>
                                </div>
                                <hr/>
                            </Fragment>
                        )}
                    </div>
                </div>
                <DccFooter/>
            </Fragment>
        );
    }
};

const mapStateToProps = (state) => {
    return {
        churchEvents: state.churchEvents,
        specialEvents: state.specialEvents
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchChurchEventsIfNeeded: () => dispatch(fetchChurchEventsIfNeeded()),
        fetchSpecialEventsIfNeeded: () => dispatch(fetchSpecialEventsIfNeeded())
    };
}

const EventsLayout = connect(
    mapStateToProps,
    mapDispatchToProps
)(Events);

export default EventsLayout;
