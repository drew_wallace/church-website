import { combineReducers } from 'redux';

import loggedIn from './loggedIn';
import curriculums from './curriculums';
import plans from './plans';
import teams from './teams';
import people from './people';
import households from './households';
import householdLeaders from './householdLeaders';
import media from './media';
import blog from './blog';
import worshipsamples from './worshipsamples';
import churchEvents from './churchEvents';
import specialEvents from './specialEvents';
import youthEvents from './youthEvents';

const App = combineReducers({
    loggedIn,
    curriculums,
    plans,
    teams,
    people,
    households,
    householdLeaders,
    media,
    blog,
    worshipsamples,
    churchEvents,
    specialEvents,
    youthEvents
});

export default App;
