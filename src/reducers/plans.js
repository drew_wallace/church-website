const plans = (state = {}, action) => {
    switch (action.type) {
        case 'INVALIDATE_PLANS':
            return Object.assign({}, state, {
                didInvalidate: true
            });
        case 'REQUEST_PLANS':
            return Object.assign({}, state, {
                isFetching: true,
                didInvalidate: false
            });
        case 'RECEIVE_PLANS':
            return Object.assign({}, state, {
                isFetching: false,
                didInvalidate: false,
                lastUpdated: action.receivedAt,
                data: action.plans
            });
        default:
            return state
    }
}

export default plans;