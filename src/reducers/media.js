const media = (state = {}, action) => {
    switch (action.type) {
        case 'INVALIDATE_MEDIA':
            return Object.assign({}, state, {
                didInvalidate: true
            });
        case 'REQUEST_MEDIA':
            return Object.assign({}, state, {
                isFetching: true,
                didInvalidate: false
            });
        case 'RECEIVE_MEDIA':
            return Object.assign({}, state, {
                isFetching: false,
                didInvalidate: false,
                lastUpdated: action.receivedAt,
                data: action.media
            });
        default:
            return state
    }
}

export default media;