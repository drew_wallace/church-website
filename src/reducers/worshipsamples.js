const worshipsamples = (state = {}, action) => {
    switch (action.type) {
        case 'INVALIDATE_WORSHIP_SAMPLES':
            return Object.assign({}, state, {
                didInvalidate: true
            });
        case 'REQUEST_WORSHIP_SAMPLES':
            return Object.assign({}, state, {
                isFetching: true,
                didInvalidate: false
            });
        case 'RECEIVE_WORSHIP_SAMPLES':
            return Object.assign({}, state, {
                isFetching: false,
                didInvalidate: false,
                lastUpdated: action.receivedAt,
                data: action.worshipsamples
            });
        default:
            return state
    }
}

export default worshipsamples;