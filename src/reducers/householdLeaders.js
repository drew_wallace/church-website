const householdLeaders = (state = {}, action) => {
    switch (action.type) {
        case 'INVALIDATE_HOUSEHOLD_LEADERS':
            return Object.assign({}, state, {
                didInvalidate: true
            });
        case 'REQUEST_HOUSEHOLD_LEADERS':
            return Object.assign({}, state, {
                isFetching: true,
                didInvalidate: false
            });
        case 'RECEIVE_HOUSEHOLD_LEADERS':
            return Object.assign({}, state, {
                isFetching: false,
                didInvalidate: false,
                lastUpdated: action.receivedAt,
                data: action.householdLeaders
            });
        default:
            return state
    }
}

export default householdLeaders;