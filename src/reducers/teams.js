const teams = (state = {}, action) => {
    switch (action.type) {
        case 'INVALIDATE_TEAMS':
            return Object.assign({}, state, {
                didInvalidate: true
            });
        case 'REQUEST_TEAMS':
            return Object.assign({}, state, {
                isFetching: true,
                didInvalidate: false
            });
        case 'RECEIVE_TEAMS':
            return Object.assign({}, state, {
                isFetching: false,
                didInvalidate: false,
                lastUpdated: action.receivedAt,
                data: action.teams
            });
        default:
            return state
    }
}

export default teams;