const blog = (state = {}, action) => {
    switch (action.type) {
        case 'INVALIDATE_BLOG':
            return Object.assign({}, state, {
                didInvalidate: true
            });
        case 'REQUEST_BLOG':
            return Object.assign({}, state, {
                isFetching: true,
                didInvalidate: false
            });
        case 'RECEIVE_BLOG':
            return Object.assign({}, state, {
                isFetching: false,
                didInvalidate: false,
                lastUpdated: action.receivedAt,
                data: action.blog
            });
        default:
            return state
    }
}

export default blog;