const specialEvents = (state = {}, action) => {
    switch (action.type) {
        case 'INVALIDATE_SPECIAL_EVENTS':
            return Object.assign({}, state, {
                didInvalidate: true
            });
        case 'REQUEST_SPECIAL_EVENTS':
            return Object.assign({}, state, {
                isFetching: true,
                didInvalidate: false
            });
        case 'RECEIVE_SPECIAL_EVENTS':
            return Object.assign({}, state, {
                isFetching: false,
                didInvalidate: false,
                lastUpdated: action.receivedAt,
                data: action.specialEvents
            });
        default:
            return state
    }
}

export default specialEvents;
