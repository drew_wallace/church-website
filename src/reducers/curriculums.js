const curriculums = (state = {}, action) => {
    switch (action.type) {
        case 'INVALIDATE_CURRICULUMS':
            return Object.assign({}, state, {
                didInvalidate: true
            });
        case 'REQUEST_CURRICULUMS':
            return Object.assign({}, state, {
                isFetching: true,
                didInvalidate: false
            });
        case 'RECEIVE_CURRICULUMS':
            return Object.assign({}, state, {
                isFetching: false,
                didInvalidate: false,
                lastUpdated: action.receivedAt,
                data: action.curriculums
            });
        default:
            return state
    }
}

export default curriculums;