const people = (state = {}, action) => {
    switch (action.type) {
        case 'INVALIDATE_PEOPLE':
            return Object.assign({}, state, {
                didInvalidate: true
            });
        case 'REQUEST_PEOPLE':
            return Object.assign({}, state, {
                isFetching: true,
                didInvalidate: false
            });
        case 'RECEIVE_PEOPLE':
            return Object.assign({}, state, {
                isFetching: false,
                didInvalidate: false,
                lastUpdated: action.receivedAt,
                data: action.people
            });
        default:
            return state
    }
}

export default people;