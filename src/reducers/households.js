const households = (state = {}, action) => {
    switch (action.type) {
        case 'INVALIDATE_HOUSEHOLDS':
            return Object.assign({}, state, {
                didInvalidate: true
            });
        case 'REQUEST_HOUSEHOLDS':
            return Object.assign({}, state, {
                isFetching: true,
                didInvalidate: false
            });
        case 'RECEIVE_HOUSEHOLDS':
            return Object.assign({}, state, {
                isFetching: false,
                didInvalidate: false,
                lastUpdated: action.receivedAt,
                data: action.households
            });
        default:
            return state
    }
}

export default households;