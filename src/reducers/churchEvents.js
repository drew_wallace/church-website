const churchEvents = (state = {}, action) => {
    switch (action.type) {
        case 'INVALIDATE_CHURCH_EVENTS':
            return Object.assign({}, state, {
                didInvalidate: true
            });
        case 'REQUEST_CHURCH_EVENTS':
            return Object.assign({}, state, {
                isFetching: true,
                didInvalidate: false
            });
        case 'RECEIVE_CHURCH_EVENTS':
            return Object.assign({}, state, {
                isFetching: false,
                didInvalidate: false,
                lastUpdated: action.receivedAt,
                data: action.churchEvents
            });
        default:
            return state
    }
}

export default churchEvents;