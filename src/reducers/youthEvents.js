const youthEvents = (state = {}, action) => {
    switch (action.type) {
        case 'INVALIDATE_YOUTH_EVENTS':
            return Object.assign({}, state, {
                didInvalidate: true
            });
        case 'REQUEST_YOUTH_EVENTS':
            return Object.assign({}, state, {
                isFetching: true,
                didInvalidate: false
            });
        case 'RECEIVE_YOUTH_EVENTS':
            return Object.assign({}, state, {
                isFetching: false,
                didInvalidate: false,
                lastUpdated: action.receivedAt,
                data: action.youthEvents
            });
        default:
            return state
    }
}

export default youthEvents;