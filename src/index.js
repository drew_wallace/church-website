import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import thunkMiddleware from 'redux-thunk';
import {createLogger} from 'redux-logger';
import {Router, Route} from 'react-router-dom';
import createBrowserHistory from 'history/createBrowserHistory';
import injectTapEventPlugin from 'react-tap-event-plugin';

import ScrollToTop from './components/scrollToTop';
import Home from './components/home';
import GetInvolved from './components/getInvolved';
import Give from './components/give';
import Gifts from './components/gifts';
import FamilyPromise from './components/familypromise';
import Youth from './components/youth';
import About from './components/about';
import Events from './components/events';
import TeacherPortal from './components/teacherPortal';
import Curriculum from './components/curriculum';
import Teams from './components/teams';
import Directory from './components/directory';
import Media from './components/media';
import WorshipSamples from './components/worshipsamples';
import PodcastAdmin from './components/podcastAdmin';

import App from './reducers';

injectTapEventPlugin();

const customHistory = createBrowserHistory();
const initialState = {
    loggedIn: false,
    curriculums: {},
    plans: {},
    teams: {},
    people: {},
    households: {},
    householdLeaders: {},
    media: {},
    blog: {},
    worshipsamples: {},
    churchEvents: {},
    specialEvents: {},
    youthEvents: {}
};

const middlewares = [thunkMiddleware];
if (process.env.NODE_ENV === `development`) {
    const loggerMiddleware = createLogger();
    middlewares.push(loggerMiddleware);
}
let store = createStore(App, initialState,
    applyMiddleware(...middlewares)
);

ReactDOM.render(
    <Provider store={store}>
        <Router history={customHistory}>
            <ScrollToTop>
                <Route exact path="/" component={Home}/>
                <Route exact path="/getinvolved" component={GetInvolved}/>
                <Route exact path="/give" component={Give}/>
                <Route exact path="/gifts" component={Gifts}/>
                <Route exact path="/familypromise" component={FamilyPromise}/>
                <Route exact path="/youth" component={Youth} />
                <Route exact path="/about" component={About} />
                <Route exact path="/events" component={Events}/>
                <Route exact path="/teacherportal" component={TeacherPortal}/>
                <Route exact path="/teacherportal/curriculum/:curriculumId" component={Curriculum} />
                <Route exact path="/teacherportal/teams" component={Teams} />
                <Route exact path="/directory" component={Directory} />
                <Route exact path="/media" component={Media} />
                {/* <Route exact path="/worshipsamples" component={WorshipSamples}/> */}
                <Route exact path="/podcastadmin" component={PodcastAdmin} />
            </ScrollToTop>
        </Router>
    </Provider>,
    document.getElementById('root')
);
