import {isEmpty} from 'lodash';

export const login = (user) => {
    return {
        type: 'LOGIN',
        user
    };
}

export const invalidateCurriculums = () => {
    return {
        type: 'INVALIDATE_CURRICULUMS'
    };
}

function requestCurriculums() {
    return {
        type: 'REQUEST_CURRICULUMS'
    };
}

function receiveCurriculums(curriculums) {
    return {
        type: 'RECEIVE_CURRICULUMS',
        curriculums,
        receivedAt: Date.now()
    };
}

function fetchCurriculums() {
    return dispatch => {
        dispatch(requestCurriculums());

        fetch('/api/curriculums', {credentials: 'include'}).then(response => {
            return response.json();
        }).then(data => {
            let curriculums = data;
            dispatch(receiveCurriculums(curriculums));
        }).catch(err => {
            console.err(err);
        });
    }
}

function shouldFetchCurriculums(state) {
    const curriculums = state.curriculums;

    if (isEmpty(curriculums)) {
        return true;
    } else if (curriculums.isFetching) {
        return false;
    } else {
        return curriculums.didInvalidate;
    }
}

export function fetchCurriculumsIfNeeded() {
    return (dispatch, getState) => {
        const state = getState();

        if (shouldFetchCurriculums(state)) {
            return dispatch(fetchCurriculums());
        } else {
            return Promise.resolve()
        }
    };
}

export const invalidatePlans = () => {
    return {
        type: 'INVALIDATE_PLANS'
    };
}

function requestPlans() {
    return {
        type: 'REQUEST_PLANS'
    };
}

function receivePlans(plans) {
    return {
        type: 'RECEIVE_PLANS',
        plans,
        receivedAt: Date.now()
    };
}

function fetchPlans() {
    return dispatch => {
        dispatch(requestPlans());

        fetch('/api/plans', {credentials: 'include'}).then(response => {
            return response.json();
        }).then(data => {
            let plans = data;
            dispatch(receivePlans(plans));
        }).catch(err => {
            console.error(err)
        });
    }
}

function shouldFetchPlans(state) {
    const plans = state.plans;

    if (isEmpty(plans)) {
        return true;
    } else if (plans.isFetching) {
        return false;
    } else {
        return plans.didInvalidate;
    }
}

export function fetchPlansIfNeeded() {
    return (dispatch, getState) => {
        const state = getState();

        if (shouldFetchPlans(state)) {
            return dispatch(fetchPlans());
        } else {
            return Promise.resolve()
        }
    };
}

export const invalidateTeams = () => {
    return {
        type: 'INVALIDATE_TEAMS'
    };
}

function requestTeams() {
    return {
        type: 'REQUEST_TEAMS'
    };
}

function receiveTeams(teams) {
    return {
        type: 'RECEIVE_TEAMS',
        teams,
        receivedAt: Date.now()
    };
}

function fetchTeams() {
    return dispatch => {
        dispatch(requestTeams());

        fetch('/api/teams', {credentials: 'include'}).then(response => {
            return response.json();
        }).then(data => {
            let teams = data;
            dispatch(receiveTeams(teams));
        }).catch(err => {
            console.error(err)
        });
    }
}

function shouldFetchTeams(state) {
    const teams = state.teams;

    if (isEmpty(teams)) {
        return true;
    } else if (teams.isFetching) {
        return false;
    } else {
        return teams.didInvalidate;
    }
}

export function fetchTeamsIfNeeded() {
    return (dispatch, getState) => {
        const state = getState();

        if (shouldFetchTeams(state)) {
            return dispatch(fetchTeams());
        } else {
            return Promise.resolve()
        }
    };
}

export const invalidatePeople = () => {
    return {
        type: 'INVALIDATE_PEOPLE'
    };
}

function requestPeople() {
    return {
        type: 'REQUEST_PEOPLE'
    };
}

function receivePeople(people) {
    return {
        type: 'RECEIVE_PEOPLE',
        people,
        receivedAt: Date.now()
    };
}

function fetchPeople() {
    return dispatch => {
        dispatch(requestPeople());

        fetch(`/api/people`, {credentials: 'include'}).then(response => {
            return response.json();
        }).then(data => {
            let people = data;
            dispatch(receivePeople(people));
        }).catch(err => {
            console.error(err)
        });
    }
}

function shouldFetchPeople(state) {
    const people = state.people;

    if (isEmpty(people)) {
        return true;
    } else if (people.isFetching) {
        return false;
    } else {
        return people.didInvalidate;
    }
}

export function fetchPeopleIfNeeded() {
    return (dispatch, getState) => {
        const state = getState();

        if (shouldFetchPeople(state)) {
            return dispatch(fetchPeople());
        } else {
            return Promise.resolve()
        }
    };
}

export const invalidateHouseholds = () => {
    return {
        type: 'INVALIDATE_HOUSEHOLDS'
    };
}

function requestHouseholds() {
    return {
        type: 'REQUEST_HOUSEHOLDS'
    };
}

function receiveHouseholds(households) {
    return {
        type: 'RECEIVE_HOUSEHOLDS',
        households,
        receivedAt: Date.now()
    };
}

function fetchHouseholds() {
    return dispatch => {
        dispatch(requestHouseholds());

        fetch(`/api/households`, {credentials: 'include'}).then(response => {
            return response.json();
        }).then(data => {
            let households = data;
            dispatch(receiveHouseholds(households));
        }).catch(err => {
            console.error(err)
        });
    }
}

function shouldFetchHouseholds(state) {
    const households = state.households;

    if (isEmpty(households)) {
        return true;
    } else if (households.isFetching) {
        return false;
    } else {
        return households.didInvalidate;
    }
}

export function fetchHouseholdsIfNeeded() {
    return (dispatch, getState) => {
        const state = getState();

        if (shouldFetchHouseholds(state)) {
            return dispatch(fetchHouseholds());
        } else {
            return Promise.resolve()
        }
    };
}

export const invalidateHouseholdLeaders = () => {
    return {
        type: 'INVALIDATE_HOUSEHOLD_LEADERS'
    };
}

function requestHouseholdLeaders() {
    return {
        type: 'REQUEST_HOUSEHOLD_LEADERS'
    };
}

function receiveHouseholdLeaders(householdLeaders) {
    return {
        type: 'RECEIVE_HOUSEHOLD_LEADERS',
        householdLeaders,
        receivedAt: Date.now()
    };
}

function fetchHouseholdLeaders() {
    return dispatch => {
        dispatch(requestHouseholdLeaders());

        fetch(`/api/householdleaders`, {credentials: 'include'}).then(response => {
            return response.json();
        }).then(data => {
            let householdLeaders = data;
            dispatch(receiveHouseholdLeaders(householdLeaders));
        }).catch(err => {
            console.error(err)
        });
    }
}

function shouldFetchHouseholdLeaders(state) {
    const householdLeaders = state.householdLeaders;

    if (isEmpty(householdLeaders)) {
        return true;
    } else if (householdLeaders.isFetching) {
        return false;
    } else {
        return householdLeaders.didInvalidate;
    }
}

export function fetchHouseholdLeadersIfNeeded() {
    return (dispatch, getState) => {
        const state = getState();

        if (shouldFetchHouseholdLeaders(state)) {
            return dispatch(fetchHouseholdLeaders());
        } else {
            return Promise.resolve()
        }
    };
}

export const invalidateMedia = () => {
    return {
        type: 'INVALIDATE_MEDIA'
    };
}

function requestMedia() {
    return {
        type: 'REQUEST_MEDIA'
    };
}

function receiveMedia(media) {
    return {
        type: 'RECEIVE_MEDIA',
        media,
        receivedAt: Date.now()
    };
}

function fetchMedia() {
    return dispatch => {
        dispatch(requestMedia());

        fetch(`/api/media`).then(response => {
            return response.json();
        }).then(data => {
            let media = data;
            dispatch(receiveMedia(media));
        }).catch(err => {
            console.error(err)
        });
    }
}

function shouldFetchMedia(state) {
    const media = state.media;

    if (isEmpty(media)) {
        return true;
    } else if (media.isFetching) {
        return false;
    } else {
        return media.didInvalidate;
    }
}

export function fetchMediaIfNeeded() {
    return (dispatch, getState) => {
        const state = getState();

        if (shouldFetchMedia(state)) {
            return dispatch(fetchMedia());
        } else {
            return Promise.resolve()
        }
    };
}

export const invalidateBlog = () => {
    return {
        type: 'INVALIDATE_BLOG'
    };
}

function requestBlog() {
    return {
        type: 'REQUEST_BLOG'
    };
}

function receiveBlog(blog) {
    return {
        type: 'RECEIVE_BLOG',
        blog,
        receivedAt: Date.now()
    };
}

function fetchBlog() {
    return dispatch => {
        dispatch(requestBlog());

        fetch(`/api/blog`).then(response => {
            return response.json();
        }).then(data => {
            let blog = data;
            dispatch(receiveBlog(blog));
        }).catch(err => {
            console.error(err)
        });
    }
}

function shouldFetchBlog(state) {
    const blog = state.blog;

    if (isEmpty(blog)) {
        return true;
    } else if (blog.isFetching) {
        return false;
    } else {
        return blog.didInvalidate;
    }
}

export function fetchBlogIfNeeded() {
    return (dispatch, getState) => {
        const state = getState();

        if (shouldFetchBlog(state)) {
            return dispatch(fetchBlog());
        } else {
            return Promise.resolve()
        }
    };
}

export const invalidateWorshipSamples = () => {
    return {
        type: 'INVALIDATE_WORSHIP_SAMPLES'
    };
}

function requestWorshipSamples() {
    return {
        type: 'REQUEST_WORSHIP_SAMPLES'
    };
}

function receiveWorshipSamples(worshipsamples) {
    return {
        type: 'RECEIVE_WORSHIP_SAMPLES',
        worshipsamples,
        receivedAt: Date.now()
    };
}

function fetchWorshipSamples() {
    return dispatch => {
        dispatch(requestWorshipSamples());

        fetch(`/api/worshipsamples`).then(response => {
            return response.json();
        }).then(data => {
            let worshipsamples = data;
            dispatch(receiveWorshipSamples(worshipsamples));
        }).catch(err => {
            console.error(err)
        });
    }
}

function shouldFetchWorshipSamples(state) {
    const worshipsamples = state.worshipsamples;

    if (isEmpty(worshipsamples)) {
        return true;
    } else if (worshipsamples.isFetching) {
        return false;
    } else {
        return worshipsamples.didInvalidate;
    }
}

export function fetchWorshipSamplesIfNeeded() {
    return (dispatch, getState) => {
        const state = getState();

        if (shouldFetchWorshipSamples(state)) {
            return dispatch(fetchWorshipSamples());
        } else {
            return Promise.resolve()
        }
    };
}

export const invalidateChurchEvents = () => {
    return {
        type: 'INVALIDATE_CHURCH_EVENTS'
    };
}

function requestChurchEvents() {
    return {
        type: 'REQUEST_CHURCH_EVENTS'
    };
}

function receiveChurchEvents(churchEvents) {
    return {
        type: 'RECEIVE_CHURCH_EVENTS',
        churchEvents,
        receivedAt: Date.now()
    };
}

function fetchChurchEvents() {
    return dispatch => {
        dispatch(requestChurchEvents());

        fetch(`/api/events/church`).then(response => {
            return response.json();
        }).then(data => {
            let churchEvents = data;
            dispatch(receiveChurchEvents(churchEvents));
        }).catch(err => {
            console.error(err)
        });
    }
}

function shouldFetchChurchEvents(state) {
    const churchEvents = state.churchEvents;

    if (isEmpty(churchEvents)) {
        return true;
    } else if (churchEvents.isFetching) {
        return false;
    } else {
        return churchEvents.didInvalidate;
    }
}

export function fetchChurchEventsIfNeeded() {
    return (dispatch, getState) => {
        const state = getState();

        if (shouldFetchChurchEvents(state)) {
            return dispatch(fetchChurchEvents());
        } else {
            return Promise.resolve()
        }
    };
}

export const invalidateSpecialEvents = () => {
    return {
        type: 'INVALIDATE_SPECIAL_EVENTS'
    };
}

function requestSpecialEvents() {
    return {
        type: 'REQUEST_SPECIAL_EVENTS'
    };
}

function receiveSpecialEvents(specialEvents) {
    return {
        type: 'RECEIVE_SPECIAL_EVENTS',
        specialEvents,
        receivedAt: Date.now()
    };
}

function fetchSpecialEvents() {
    return dispatch => {
        dispatch(requestSpecialEvents());

        fetch(`/api/events/special`).then(response => {
            return response.json();
        }).then(data => {
            let specialEvents = data;
            dispatch(receiveSpecialEvents(specialEvents));
        }).catch(err => {
            console.error(err)
        });
    }
}

function shouldFetchSpecialEvents(state) {
    const specialEvents = state.specialEvents;

    if (isEmpty(specialEvents)) {
        return true;
    } else if (specialEvents.isFetching) {
        return false;
    } else {
        return specialEvents.didInvalidate;
    }
}

export function fetchSpecialEventsIfNeeded() {
    return (dispatch, getState) => {
        const state = getState();

        if (shouldFetchSpecialEvents(state)) {
            return dispatch(fetchSpecialEvents());
        } else {
            return Promise.resolve()
        }
    };
}

export const invalidateYouthEvents = () => {
    return {
        type: 'INVALIDATE_YOUTH_EVENTS'
    };
}

function requestYouthEvents() {
    return {
        type: 'REQUEST_YOUTH_EVENTS'
    };
}

function receiveYouthEvents(youthEvents) {
    return {
        type: 'RECEIVE_YOUTH_EVENTS',
        youthEvents,
        receivedAt: Date.now()
    };
}

function fetchYouthEvents() {
    return dispatch => {
        dispatch(requestYouthEvents());

        fetch(`/api/events/youth`).then(response => {
            return response.json();
        }).then(data => {
            let youthEvents = data;
            dispatch(receiveYouthEvents(youthEvents));
        }).catch(err => {
            console.error(err)
        });
    }
}

function shouldFetchYouthEvents(state) {
    const youthEvents = state.youthEvents;

    if (isEmpty(youthEvents)) {
        return true;
    } else if (youthEvents.isFetching) {
        return false;
    } else {
        return youthEvents.didInvalidate;
    }
}

export function fetchYouthEventsIfNeeded() {
    return (dispatch, getState) => {
        const state = getState();

        if (shouldFetchYouthEvents(state)) {
            return dispatch(fetchYouthEvents());
        } else {
            return Promise.resolve()
        }
    };
}
