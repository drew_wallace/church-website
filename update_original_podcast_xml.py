import xml.etree.cElementTree
import requests
import pydash

response = requests.get('http://mediaplayer.cloversites.com/v2/players/3805f4e4-1553-45ea-be76-06878338ba98')
medias = pydash.collections.key_by(response.json()['player']['media'], 'id')

ns = {
	'itunes': 'http://www.itunes.com/dtds/podcast-1.0.dtd',
	'googleplay': 'http://www.google.com/schemas/play-podcasts/1.0/play-podcasts.xsd',
	'atom': 'http://www.w3.org/2005/Atom'
}

xml.etree.cElementTree.register_namespace('itunes', 'http://www.itunes.com/dtds/podcast-1.0.dtd')
xml.etree.cElementTree.register_namespace('googleplay', 'http://www.google.com/schemas/play-podcasts/1.0/play-podcasts.xsd')
xml.etree.cElementTree.register_namespace('atom', 'http://www.w3.org/2005/Atom')

tree = xml.etree.cElementTree.parse('podcast.xml')
root = tree.getroot()
channel = root.find('channel')
# print(channel.find('itunes:owner', ns).find('itunes:name', ns).text)
for item in channel.findall('item'):
	id = item.find('guid').text.replace('http://dunwoodychurch.org/media?id=', '')
	item.find('itunes:author', ns).text = medias[int(id)]['speaker']
	item.find('itunes:subtitle', ns).text = medias[int(id)]['series']
	item.find('itunes:summary', ns).text = medias[int(id)]['series']
	item.find('enclosure').set('url', 'http://dunwoodychurch.org/podcast/' + id + '.' + medias[int(id)]['download_url'][-3:].lower())
	item.find('itunes:image', ns).set('href', 'http://dunwoodychurch.org/podcast/' + id + '.' + medias[int(id)]['thumbnails']['key'][-3:].lower())

tree.write('podcast.xml')