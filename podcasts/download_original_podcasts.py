from tqdm import tqdm
import requests
import os

def download_file(url, filename):
    testread = requests.head(url)     # A HEAD request only downloads the headers
    filelength = int(testread.headers['Content-length'])

    r = requests.get(url, stream=True)  # actual download full file

    with open(filename, 'wb') as f:
        pbar = tqdm(total=int(filelength/1024))
        for chunk in r.iter_content(chunk_size=1024):
            if chunk:                   # filter out keep-alive new chunks
                pbar.update ()
                f.write(chunk)

response = requests.get('http://mediaplayer.cloversites.com/v2/players/3805f4e4-1553-45ea-be76-06878338ba98')
for media in response.json()['player']['media']:
	# guid = media['thumbnails']['key'].replace('.jpg', '').replace('.JPG', '').replace('.png', '')
    id = str(media['id'])


    url = media['thumbnails']['original']

    try:
        os.listdir(os.getcwd()).index(id + '.' + url[-3:].lower())
        print('found', id + '.' + url[-3:].lower())
    except ValueError:
        download_file(url, id + '.' + url[-3:].lower())
        url = media['download_url']
        download_file(url, id + '.' + url[-3:].lower())
